<?php /* Smarty version Smarty-3.1.21-dev, created on 2017-11-20 20:04:08
         compiled from "/var/www/html/ciTemplate/application/modules/front/views/respon/home_body.tpl" */ ?>
<?php /*%%SmartyHeaderCode:152714037359fc0c3fc15ad1-43681876%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '8fcff6ca45f7aee49181ef2fa47d1cf71683cf41' => 
    array (
      0 => '/var/www/html/ciTemplate/application/modules/front/views/respon/home_body.tpl',
      1 => 1509690358,
      2 => 'file',
    ),
    '02f94ccc4f456363a16384c26bb4142a8342d0a6' => 
    array (
      0 => '/var/www/html/ciTemplate/application/modules/front/views/home.tpl',
      1 => 1510373282,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '152714037359fc0c3fc15ad1-43681876',
  'function' => 
  array (
  ),
  'cache_lifetime' => 3600,
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_59fc0c408eb8e7_48029338',
  'variables' => 
  array (
    'this' => 0,
    'title' => 0,
    'css_plugin' => 0,
    'v' => 0,
    'css' => 0,
    'users' => 0,
    'body' => 1,
    'js_plugin' => 0,
    'js' => 0,
  ),
  'has_nocache_code' => true,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_59fc0c408eb8e7_48029338')) {function content_59fc0c408eb8e7_48029338($_smarty_tpl) {?><!DOCTYPE html>
<html>

<head>
   <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Favicon-->
        <link rel="icon" href="favicon.ico" type="image/x-icon">
        <!-- Google Fonts -->
        
        <link href="<?php echo $_smarty_tpl->tpl_vars['this']->value->parser->theme_url('css/material_font/material-icons.css');?>
" rel="stylesheet" type="text/css">
       
        <title><?php echo $_smarty_tpl->tpl_vars['title']->value;?>
 GTLN</title>
        
        <link href="<?php echo $_smarty_tpl->tpl_vars['this']->value->parser->theme_url('plugins/bootstrap/css/bootstrap.css');?>
" rel="stylesheet">
        <link href="<?php echo $_smarty_tpl->tpl_vars['this']->value->parser->theme_url('plugins/node-waves/waves.css');?>
" rel="stylesheet">
        <link href="<?php echo $_smarty_tpl->tpl_vars['this']->value->parser->theme_url('plugins/animate-css/animate.css');?>
" rel="stylesheet">

        <?php  $_smarty_tpl->tpl_vars['v'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['v']->_loop = false;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['css_plugin']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['v']->key => $_smarty_tpl->tpl_vars['v']->value) {
$_smarty_tpl->tpl_vars['v']->_loop = true;
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['v']->key;
?>
          <link href="<?php echo $_smarty_tpl->tpl_vars['v']->value;?>
" rel="stylesheet">
        <?php } ?>
        <?php  $_smarty_tpl->tpl_vars['v'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['v']->_loop = false;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['css']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['v']->key => $_smarty_tpl->tpl_vars['v']->value) {
$_smarty_tpl->tpl_vars['v']->_loop = true;
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['v']->key;
?>
        <?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['v']->value;?>
<?php $_tmp1=ob_get_clean();?><?php echo css($_tmp1);?>

        <?php } ?>
        
        <link href="<?php echo $_smarty_tpl->tpl_vars['this']->value->parser->theme_url('css/themes/all-themes.css');?>
" rel="stylesheet" type="text/css">
</head>

<body class="theme-light-blue">
    
    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-red">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p>Please wait...</p>
        </div>
    </div>
    </div>
    <!-- #END# Search Bar -->
    <!-- Top Bar -->
    <nav class="navbar">
        <div class="container-fluid">
            <div class="navbar-header">
                <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
                <a href="javascript:void(0);" class="bars"></a>
                <a class="navbar-brand" href="/">ADMINBSB - MATERIAL DESIGN</a>
            </div>
            <div class="collapse navbar-collapse" id="navbar-collapse">
               
            </div>
        </div>
    </nav>
    <!-- #Top Bar -->
    <section>
        <!-- Left Sidebar -->
        <aside id="leftsidebar" class="sidebar">
            <!-- User Info -->
            <div class="user-info">
                <div class="image">
                    <img src="<?php echo base_url();?>
assets/images/user.png" width="48" height="48" alt="<?php echo $_smarty_tpl->tpl_vars['users']->value->first_name;?>
" />
                </div>
                <div class="info-container">
                    <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php echo $_smarty_tpl->tpl_vars['users']->value->first_name;?>
</div>
                    <div class="email"><?php echo $_smarty_tpl->tpl_vars['users']->value->username;?>
</div>
                    <div class="btn-group user-helper-dropdown">
                        <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
                        <ul class="dropdown-menu pull-right">
                            <li><a href="javascript:void(0);"><i class="material-icons">person</i>Profile</a></li>
                            <li role="seperator" class="divider"></li>
                            <li><a href="javascript:void(0);"><i class="material-icons">group</i>Followers</a></li>
                            <li><a href="javascript:void(0);"><i class="material-icons">shopping_cart</i>Sales</a></li>
                            <li><a href="javascript:void(0);"><i class="material-icons">favorite</i>Likes</a></li>
                            <li role="seperator" class="divider"></li>
                            <li><a href="javascript:void(0);"><i class="material-icons">input</i>Sign Out</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- #User Info -->
            <!-- Menu -->
            <div class="menu">
                <ul class="list">
                    <li class="header">MAIN NAVIGATION</li>
                    <li class="active">
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons">content_copy</i>
                            <span>Log Record</span>
                        </a>
                        <ul class="ml-menu">
                            <li>
                                <a href="<?php echo base_url();?>
front/dasbord/log_respon">Respon BC</a>
                            </li>
                            <li>
                                <a href="<?php echo base_url();?>
front/dasbord/lazadaTracking">Tracking Lazada</a>
                            </li>
                            <li>
                                <a href="#">Forgot Password</a>
                            </li>
                            <li>
                                <a href="#">Blank Page</a>
                            </li>
                            <li>
                                <a href="#">404 - Not Found</a>
                            </li>
                            <li>
                                <a href="#">500 - Server Error</a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </aside>
        <!-- #END# Left Sidebar -->
    </section>

    <section class="content">
    <ol class="breadcrumb breadcrumb-bg-pink clearfix">
        <li><a href="javascript:void(0);"><i class="material-icons">home</i> Home</a></li>
        <li class="active"><i class="material-icons">library_books</i> <strong><?php echo $_smarty_tpl->tpl_vars['title']->value;?>
</strong></li>
    </ol>
        <div class="container-fluid">
            <div class="block-header">
                
                
 	<!-- Real-Time Chart -->
            <div class="row clearfix">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="card">
                        <div class="header">
                            <h2>REAL-TIME CHART</h2>
                            <div class="pull-right">
                                <div class="switch panel-switch-btn">
                                    <span class="m-r-10 font-12">REAL TIME</span>
                                    <label>OFF<input type="checkbox" id="realtime" checked><span class="lever switch-col-cyan"></span>ON</label>
                                </div>
                            </div>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="javascript:void(0);">Action</a></li>
                                        <li><a href="javascript:void(0);">Another action</a></li>
                                        <li><a href="javascript:void(0);">Something else here</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="body">
                            <div id="real_time_chart" class="flot-chart"></div>
                        </div>
                    </div>
                </div>
            </div>
 
                
            </div>
        </div>
    </section>
    <?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['this']->value->parser->theme_url('plugins/jquery/jquery.min.js');?>
"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['this']->value->parser->theme_url('plugins/bootstrap/js/bootstrap.js');?>
"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['this']->value->parser->theme_url('plugins/bootstrap-select/js/bootstrap-select.js');?>
"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['this']->value->parser->theme_url('plugins/jquery-slimscroll/jquery.slimscroll.js');?>
"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['this']->value->parser->theme_url('plugins/node-waves/waves.js');?>
"><?php echo '</script'; ?>
>
   <?php  $_smarty_tpl->tpl_vars['v'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['v']->_loop = false;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['js_plugin']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['v']->key => $_smarty_tpl->tpl_vars['v']->value) {
$_smarty_tpl->tpl_vars['v']->_loop = true;
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['v']->key;
?>
    <?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['v']->value;?>
"><?php echo '</script'; ?>
>
    <?php } ?>
    <?php  $_smarty_tpl->tpl_vars['v'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['v']->_loop = false;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['js']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['v']->key => $_smarty_tpl->tpl_vars['v']->value) {
$_smarty_tpl->tpl_vars['v']->_loop = true;
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['v']->key;
?>
    <?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['v']->value;?>
<?php $_tmp2=ob_get_clean();?><?php echo js($_tmp2);?>

    <?php } ?>
</body>

</html>
<?php }} ?>
