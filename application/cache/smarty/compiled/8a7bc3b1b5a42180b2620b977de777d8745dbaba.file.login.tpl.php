<?php /* Smarty version Smarty-3.1.21-dev, created on 2018-01-07 18:35:37
         compiled from "/var/www/html/gtln2.0/application/modules/front/views/auth/login.tpl" */ ?>
<?php /*%%SmartyHeaderCode:14758297425a52060961a1f5-03444651%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '8a7bc3b1b5a42180b2620b977de777d8745dbaba' => 
    array (
      0 => '/var/www/html/gtln2.0/application/modules/front/views/auth/login.tpl',
      1 => 1509520358,
      2 => 'file',
    ),
    'ff36a0acf182b76b5706585b79a4712fbf32e41e' => 
    array (
      0 => '/var/www/html/gtln2.0/application/modules/front/views/index.tpl',
      1 => 1510135121,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '14758297425a52060961a1f5-03444651',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'this' => 0,
    'title' => 0,
    'css_plugin' => 0,
    'v' => 0,
    'css' => 0,
    'body_class' => 0,
    'body' => 0,
    'js_plugin' => 0,
    'js' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_5a5206096f1838_97100742',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5a5206096f1838_97100742')) {function content_5a5206096f1838_97100742($_smarty_tpl) {?><!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Favicon-->
        <link rel="icon" href="favicon.ico" type="image/x-icon">
        <!-- Google Fonts -->
        
        <link href="<?php echo $_smarty_tpl->tpl_vars['this']->value->parser->theme_url('css/material_font/material-icons.css');?>
" rel="stylesheet" type="text/css">

        <title><?php echo $_smarty_tpl->tpl_vars['title']->value;?>
 GTLN</title>
        
        <link href="<?php echo $_smarty_tpl->tpl_vars['this']->value->parser->theme_url('plugins/bootstrap/css/bootstrap.css');?>
" rel="stylesheet">
        <link href="<?php echo $_smarty_tpl->tpl_vars['this']->value->parser->theme_url('plugins/node-waves/waves.css');?>
" rel="stylesheet">
        <link href="<?php echo $_smarty_tpl->tpl_vars['this']->value->parser->theme_url('plugins/animate-css/animate.css');?>
" rel="stylesheet">

        <?php  $_smarty_tpl->tpl_vars['v'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['v']->_loop = false;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['css_plugin']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['v']->key => $_smarty_tpl->tpl_vars['v']->value) {
$_smarty_tpl->tpl_vars['v']->_loop = true;
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['v']->key;
?>
          <link href="<?php echo $_smarty_tpl->tpl_vars['v']->value;?>
" rel="stylesheet">
        <?php } ?>
        <?php  $_smarty_tpl->tpl_vars['v'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['v']->_loop = false;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['css']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['v']->key => $_smarty_tpl->tpl_vars['v']->value) {
$_smarty_tpl->tpl_vars['v']->_loop = true;
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['v']->key;
?>
        <?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['v']->value;?>
<?php $_tmp1=ob_get_clean();?><?php echo css($_tmp1);?>

        <?php } ?>
        <link href="<?php echo $_smarty_tpl->tpl_vars['this']->value->parser->theme_url('css/themes/all-themes.css');?>
" rel="stylesheet" type="text/css">
        
    </head>
    <body class="<?php echo $_smarty_tpl->tpl_vars['body_class']->value;?>
">
        
    <div class="login-box">
        <div class="logo">
            <a href="javascript:void(0);">Admin<b>BSB</b></a>
            <small>Admin BootStrap Based - Material Design</small>
        </div>
        <div class="card">
            <div class="body">
                <form id="sign_in">
                    <div class="msg">Sign in to start your session</div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">person</i>
                        </span>
                        <div class="form-line">
                            <input type="text" class="form-control" name="username" placeholder="Username" required autofocus>
                        </div>
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">lock</i>
                        </span>
                        <div class="form-line">
                            <input type="password" class="form-control" name="password" placeholder="Password" required>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-8 p-t-5">
                            <input type="checkbox" name="rememberme" id="rememberme" class="filled-in chk-col-pink">
                            <label for="rememberme">Remember Me</label>
                        </div>
                        <div class="col-xs-4">
                            <button class="btn btn-block bg-pink waves-effect" type="submit">SIGN IN</button>
                        </div>
                    </div>
                    <div class="row m-t-15 m-b--20">
                        <div class="col-xs-6">
                            <a href="sign-up.html">Register Now!</a>
                        </div>
                        <div class="col-xs-6 align-right">
                            <a href="forgot-password.html">Forgot Password?</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    
        <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-red">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p>Please wait...</p>
        </div>
    </div>
    <?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['this']->value->parser->theme_url('plugins/jquery/jquery.min.js');?>
"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['this']->value->parser->theme_url('plugins/bootstrap/js/bootstrap.js');?>
"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['this']->value->parser->theme_url('plugins/node-waves/waves.js');?>
"><?php echo '</script'; ?>
>
   <?php  $_smarty_tpl->tpl_vars['v'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['v']->_loop = false;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['js_plugin']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['v']->key => $_smarty_tpl->tpl_vars['v']->value) {
$_smarty_tpl->tpl_vars['v']->_loop = true;
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['v']->key;
?>
    <?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['v']->value;?>
"><?php echo '</script'; ?>
>
    <?php } ?>
    <?php  $_smarty_tpl->tpl_vars['v'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['v']->_loop = false;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['js']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['v']->key => $_smarty_tpl->tpl_vars['v']->value) {
$_smarty_tpl->tpl_vars['v']->_loop = true;
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['v']->key;
?>
    <?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['v']->value;?>
<?php $_tmp2=ob_get_clean();?><?php echo js($_tmp2);?>

    <?php } ?>
        

    </body>
</html>
<?php }} ?>
