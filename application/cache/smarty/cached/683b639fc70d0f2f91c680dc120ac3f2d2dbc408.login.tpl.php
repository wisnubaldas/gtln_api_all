<?php /*%%SmartyHeaderCode:173164994259f998e54e9556-74589244%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '683b639fc70d0f2f91c680dc120ac3f2d2dbc408' => 
    array (
      0 => '/var/www/html/ciTemplate/application/modules/front/views/auth/login.tpl',
      1 => 1509520358,
      2 => 'file',
    ),
    '28dba978cade1a0738ee03d25edd92789db44262' => 
    array (
      0 => '/var/www/html/ciTemplate/application/modules/front/views/index.tpl',
      1 => 1510135121,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '173164994259f998e54e9556-74589244',
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_5a12d2baaf3d27_01801398',
  'has_nocache_code' => false,
  'cache_lifetime' => 3600,
),true); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5a12d2baaf3d27_01801398')) {function content_5a12d2baaf3d27_01801398($_smarty_tpl) {?><!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Favicon-->
        <link rel="icon" href="favicon.ico" type="image/x-icon">
        <!-- Google Fonts -->
        
        <link href="http://gtln.dev/assets/css/material_font/material-icons.css" rel="stylesheet" type="text/css">

        <title>Login Pages GTLN</title>
        
        <link href="http://gtln.dev/assets/plugins/bootstrap/css/bootstrap.css" rel="stylesheet">
        <link href="http://gtln.dev/assets/plugins/node-waves/waves.css" rel="stylesheet">
        <link href="http://gtln.dev/assets/plugins/animate-css/animate.css" rel="stylesheet">

                        <link rel="stylesheet" type="text/css" href="http://gtln.dev/assets/css/materialize.css" media="screen">
                <link rel="stylesheet" type="text/css" href="http://gtln.dev/assets/css/style.css" media="screen">
                <link href="http://gtln.dev/assets/css/themes/all-themes.css" rel="stylesheet" type="text/css">
        
    </head>
    <body class="login-page">
        
    <div class="login-box">
        <div class="logo">
            <a href="javascript:void(0);">Admin<b>BSB</b></a>
            <small>Admin BootStrap Based - Material Design</small>
        </div>
        <div class="card">
            <div class="body">
                <form id="sign_in">
                    <div class="msg">Sign in to start your session</div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">person</i>
                        </span>
                        <div class="form-line">
                            <input type="text" class="form-control" name="username" placeholder="Username" required autofocus>
                        </div>
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">lock</i>
                        </span>
                        <div class="form-line">
                            <input type="password" class="form-control" name="password" placeholder="Password" required>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-8 p-t-5">
                            <input type="checkbox" name="rememberme" id="rememberme" class="filled-in chk-col-pink">
                            <label for="rememberme">Remember Me</label>
                        </div>
                        <div class="col-xs-4">
                            <button class="btn btn-block bg-pink waves-effect" type="submit">SIGN IN</button>
                        </div>
                    </div>
                    <div class="row m-t-15 m-b--20">
                        <div class="col-xs-6">
                            <a href="sign-up.html">Register Now!</a>
                        </div>
                        <div class="col-xs-6 align-right">
                            <a href="forgot-password.html">Forgot Password?</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    
        <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-red">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p>Please wait...</p>
        </div>
    </div>
    <script src="http://gtln.dev/assets/plugins/jquery/jquery.min.js"></script>
    <script src="http://gtln.dev/assets/plugins/bootstrap/js/bootstrap.js"></script>
    <script src="http://gtln.dev/assets/plugins/node-waves/waves.js"></script>
       <script src="http://gtln.dev/assets/plugins/jquery-validation/jquery.validate.js"></script>
            <script type="text/javascript" src="http://gtln.dev/assets/js/admin.js"></script>
        <script type="text/javascript" src="http://gtln.dev/assets/js/pages/examples/sign-in.js"></script>
            

    </body>
</html>
<?php }} ?>
