<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tr_status_send_model extends MY_Model {
public function __construct()
{
		$this->table = 'tr_status_send';
        $this->primary_key = 'id';
        $this->has_many['tr_pkg_status'] = array('foreign_model'=>'Tr_pkg_status_model','foreign_table'=>'tr_pkg_status','foreign_key'=>'id','local_key'=>'id');
        $this->return_as = 'object';
        $this->timestamps = array('update_at','create_at');
	parent::__construct();
	
}
	

}

/* End of file Tr_status_send.php */
/* Location: ./application/modules/lzd/models/Tr_status_send.php */