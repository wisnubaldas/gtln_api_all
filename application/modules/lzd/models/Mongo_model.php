<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mongo_model extends CI_Model {
public function __construct()
{
	parent::__construct();
	$this->load->library('mongo_db');
	$this->mongo_db->reconnect([
				    'config' => [
				        'connection' => [
				            'host' => ['localhost'],
				            'port' => [],
				            'user_name' => '',
							'user_password' => '',
				            'db_name' => 'tracking_lazada',
				        ]
				    ]
				]);
}
	
public function get_stat_tracking()
{
	$return  = $this->mongo_db
						->limit(5)
						->get('send_arrival');
						print_r($return);
}

}// end classs

/* End of file Mongo_model.php */
/* Location: ./application/modules/lzd/models/Mongo_model.php */