<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tr_pkg_status_model extends MY_Model {
public function __construct()
{
		$this->table = 'tr_pkg_status';
        $this->primary_key = 'id';
        $this->has_one['tr_status_send'] = array('foreign_model'=>'Tr_status_send_model','foreign_table'=>'tr_status_send','foreign_key'=>'id','local_key'=>'id');
        $this->has_one['tr_getout'] = array('foreign_model'=>'tr_getout_model','foreign_table'=>'tr_getout','foreign_key'=>'hawb','local_key'=>'hawb');
        $this->return_as = 'object';
        $this->timestamps = array('update_at','create_at');
	parent::__construct();
	
}
	

}

/* End of file Tr_pkg_status_model.php */
/* Location: ./application/modules/lzd/models/Tr_pkg_status_model.php */