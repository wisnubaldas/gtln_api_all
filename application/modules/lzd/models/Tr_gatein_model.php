<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tr_gatein_model extends MY_Model {
public function __construct()
        {
        	$this->table = 'tr_gatein';
                $this->primary_key = 'hawb';
                
                $this->has_one['bc_t_shipment'] = 'Bc_t_shipment_model';
                $this->has_one['bc_respone_400'] = array('foreign_model'=>'bc_respone_400_model','foreign_table'=>'bc_respone_400','foreign_key'=>'NO_BARANG','local_key'=>'hawb');
                $this->has_one['tr_gateout'] = array('foreign_model'=>'Tr_gateout_model','foreign_table'=>'tr_gateout','foreign_key'=>'hawb','local_key'=>'hawb');
                $this->return_as = 'object';
                $this->timestamps = array('updated_at','created_at');
                // $this->before_get[] = '_uper_case';
        	parent::__construct();
        	
        }
// protected function _uper_case($hawb)
//         {
//                 $x = strtoupper($hawb['hawb']);
//                 return $x;
//         }
}

/* End of file Tr_gatein_model.php */
/* Location: ./application/modules/lzd/models/Tr_gatein_model.php */