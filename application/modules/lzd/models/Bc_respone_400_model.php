<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bc_respone_400_model extends MY_Model {
public function __construct()
{
		$this->table = 'bc_respone_400';
        $this->primary_key = 'NO_BARANG';
        $this->has_one['tr_gatein'] = array('foreign_model'=>'Tr_gatein_model','foreign_table'=>'tr_gatein','foreign_key'=>'hawb','local_key'=>'NO_BARANG');
        $this->return_as = 'object';
        $this->timestamps = array('update_at','create_at');
	parent::__construct();
	
}

}

/* End of file Bc_respone_400_model.php */
/* Location: ./application/modules/lzd/models/Bc_respone_400_model.php */