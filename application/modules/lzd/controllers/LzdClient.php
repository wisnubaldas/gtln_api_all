<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;

// maksimal 2 hari dari arrival, arrival harus tau time stam bc11 sebagai landasan , 

class LzdClient extends MX_Controller {

	public $url;
	public $date_time;
	public $user_id;
	public $key;
	public $action;
	protected $client;

	public function __construct()
	{
		parent::__construct();
		// date_default_timezone_set('Asia/Jakarta');
		$this->url = 'https://crossborder.lazada.com/api/3pl/v2/?'; // production lazada
		// $this->url = 'https://crossborder.lazada.com/api-staging/3pl/v2?'; // development lazada
		$this->date_time = $this->atom_date(date('Y-m-d H:i:s'));
		$this->user_id = 'GTLN';
		$this->key = '0NEOFNLDFJDK1';
		$this->action = 'StatusUpdate';
		// load Model etc
		// $this->load->model('lzd/req_pkg_status');
		// $this->load->model('lzd/tr_gatein');
		$model = ['tr_gatein_model','tr_status_send_model','tr_pkg_status_model','bc_respone_400_model','tr_gateout_model'];
		$this->load->model($model);
		// load library 
		$this->client = new \GuzzleHttp\Client(['cookies' => true]);
	}

	protected function waktu($waktu = "now",$zone = "Asia/Jakarta")
	{
		return new DateTime($waktu, new DateTimeZone($zone));
	}

	protected function cek_status_arr_bc($data)
	{
		foreach ($data as $v) {
			if ((string)$v->status == 'handovered') {
				return true;
			}
		}
	}

	public function hand_over()
	{
		$dtScan = $this->tr_gateout_model
					->where(['istransfer'=>0])
					->with_bc_t_shipment('fields:Weight')
					->with_tr_pkg_status('fields:hawb,status,create_at')
					->fields('hawb,scanout')
					// ->order_by('rand()')
					->limit(300)
					->get_all();
				if(!$dtScan)
					{
						$this->tr_gateout_model->where('istransfer',5)->update(['istransfer'=>0]);
						exit();
					}

		$this->data_over = [];
		// cari selisih waktu
		$sekarang = new DateTime("now", new DateTimeZone("Asia/Jakarta"));
		// echo $sekarang->format('Y-m-d H:m:s'), PHP_EOL;
		foreach ($dtScan as $v) {
			$kemarin = new DateTime($v->scanout, new DateTimeZone("Asia/Jakarta"));
			// echo $kemarin->format('Y-m-d H:m:s'),PHP_EOL;
			$selisihnya=$sekarang->diff($kemarin);
			// echo $selisihnya->days, PHP_EOL; 
		// house yg belom pernah di kirim ke lazada dan 
		// lebih dari 3 hari kasih flag 4
		if($selisihnya->days > 3)
		{
			$this->tr_gateout_model->where('hawb',$v->hawb)->update(['istransfer'=>4]);
		}

		if (!array_key_exists('tr_pkg_status', (array)$v)) {
			// sama sekali belom pernah terkirim ke lazada
			$this->tr_gateout_model->where('hawb',$v->hawb)->update(['istransfer'=>5]);
		}elseif (!array_key_exists('bc_t_shipment', (array)$v)) {
			// ngga ada di shipmet adanya dimana...????
			$this->tr_gateout_model->where('hawb',$v->hawb)->update(['istransfer'=>6]);
		}elseif ($v->tr_pkg_status) {
			// datanya sudah lengkap
			foreach ($v->tr_pkg_status as $vs) {
				if ($vs->status == 'import_custom_clearance_success') {
					$tracking_number = $v->hawb;
					$status = 'handovered';
					$status_date = $this->hongkong_date($v->scanout);
					$weight_kg = $v->bc_t_shipment->Weight;
					array_push($this->data_over, compact('tracking_number','status','status_date','weight_kg'));
				}
			}
		}
			}
		if(count($this->data_over) == 0)
				{
					// tarikan dari limit data tidak lengkap 
					echo "data tarikan kosong ";
					exit();
				}
		$this->data = [
						'msg_type'=>'PACKAGE_STATUS',
						'package'=>$this->data_over
				  	  	];
				// kirim data ke API Lazada
				$lazadaRes = $this->send_lazada(json_encode($this->data));
				log_message('error','Hand Over'.json_encode($lazadaRes));
				// // baca  error respon dari lazada
				$respon = $this->cek_error_respon($lazadaRes);
				print_r($lazadaRes);
				// print_r('<div>');
				// print_r(json_encode($this->data),JSON_PRETTY_PRINT);

				// parsing data respon lazada
				if($respon !== true)	
				{
					$request_time = $lazadaRes->response->request_time;
					$total_request = $lazadaRes->response->records->total;
					$new_request = $lazadaRes->response->records->new;
					$update_request = $lazadaRes->response->records->update;
					$id = $this->tr_status_send_model
							->insert(compact('request_time','total_request','new_request','update_request'));
					foreach ($this->data_over as $v) {
						$dataNya = ['id'=>$id,'hawb'=>$v['tracking_number'],'status'=>$v['status'],'time_host'=>$v['status_date']];
						$this->tr_pkg_status_model->insert($dataNya);
						$this->tr_gateout_model->where('hawb',$v['tracking_number'])->update(['istransfer'=>1]);
						/// kirim notif ke cloud 
					}
					// $xx = $this->send_cloud_dbrespon($this->data_over,$request_time);
					// log_message('error',(string)$xx);	
					// log_message('error','HO'.json_encode($lazadaRes));
				}
	} // hand over

	public function success_bc()
	{
//		cari data arrival yg sudah terkirim status tracking di flag 1 gatein
		$dtScan = $this->bc_respone_400_model
					->where(['flag_lzd'=>0]) // 400 belom terkirim
					->with_tr_gatein('fields:hawb,tglscan,idkey','where:`istransfer`=\'1\'') // srrived sudah terkirim
					->fields('NO_BARANG,KD_RESPON,WK_REKAM,id_res_cn')
					->order_by('rand()')
					->limit(500)
					->get_all();
					
					
					if(!$dtScan) // jika flag suda tidak ada
					{
						$where = ['flag_lzd'=>6];
						$this->bc_respone_400_model->where($where)->update(['flag_lzd'=>0]); // balikin ke 0 supaya bisa di cek ulang
						exit();
					}
			// prepare data yg mau dikirim
			$data_bc = array();
			$id_respon400 = array();

			foreach ($dtScan as $v) {
				// print_r($v->tr_gatein);
				// exit();
				if (!property_exists($v->tr_gatein,'hawb')) {
					$where = ['id_res_cn'=>$v->id_res_cn];
					$this->bc_respone_400_model->where($where)->update(['flag_lzd'=>6]); // update ke 6 misahin data yg ngga ada di arrivaed
				}
				if(in_array($v->KD_RESPON, ['405','406','408']))
				{
					$where = ['id_res_cn'=>$v->id_res_cn];
					$this->bc_respone_400_model->where($where)->update(['flag_lzd'=>5]);
				}
				if(in_array($v->KD_RESPON, ['403','401']))
				{
					$tracking_number = $v->NO_BARANG;
					$status = 'import_custom_clearance_success';
					$status_date = $this->hongkong_date($v->WK_REKAM);
					array_push($data_bc, compact('tracking_number','status','status_date'));
					array_push($id_respon400, ['id_res_cn'=>$v->id_res_cn]);
				}
			}
			if(count($data_bc) == 0)
				{
					echo "data kosong";
					exit();
				}
			// keluarin
				// print_r($dtScan);
				// 	exit();
			$this->data = [
						'msg_type'=>'PACKAGE_STATUS',
						'package'=>$data_bc
				  	  	];

				// kirim data ke API Lazada
				$lazadaRes = $this->send_lazada(json_encode($this->data));
				// // cek error respon lazada
				$respon = $this->cek_error_respon($lazadaRes);
				log_message('error','Sukses BC'.json_encode($lazadaRes));
				print_r($lazadaRes);
				if($respon !== true)
				{
					$request_time = $lazadaRes->response->request_time;
					$total_request = $lazadaRes->response->records->total;
					$new_request = $lazadaRes->response->records->new;
					$update_request = $lazadaRes->response->records->update;
					$id = $this->tr_status_send_model
							->insert(compact('request_time','total_request','new_request','update_request'));
					foreach ($data_bc as $v) {
						$dataNya = ['id'=>$id,'hawb'=>$v['tracking_number'],'status'=>$v['status'],'time_host'=>$v['status_date']];
						$this->tr_pkg_status_model->insert($dataNya);
						// update gatein arrival jika dar 1 ke 2
						// flag tr gatein = 2 (sukses respon bc)
						$this->tr_gatein_model->where('hawb',$v['tracking_number'])->update(['istransfer'=>2]);
					}
					// update respon 400
					foreach ($id_respon400 as $v) {
						$this->bc_respone_400_model->where('id_res_cn',$v['id_res_cn'])->update(['flag_lzd'=>1]);
					}
					// $xx = $this->send_cloud_dbrespon($this->data);
					// log_message('error',(string)$xx);
				}
	}

	public function arrived()
	{
		$dtScan = $this->tr_gatein_model
				->where(['istransfer'=>0])
				->with_bc_t_shipment('fields:hawb')
				->fields('hawb,tglscan,idkey,status')
				->limit(400)
				->get_all();
				print_r($this->texs($dtScan));
				exit();

		if(!$dtScan)
		{
			echo "string";
			exit();
		}
		// print_r($dtScan);

		$this->data_arrived = [];
		foreach ($dtScan as $v) {
			// echo strpbrk($v->hawb, 'id');
			// cek interval jika lebih dari 8 jam buat wakru sekarang
			$intrv = $this->intervalDate($v->tglscan,date("Y-m-d H:i:s"));
			if((int)$intrv > 9)
			{
				 $v->tglscan = date("Y-m-d H:i:s");	
			}

			// jika tidak ada host di shipment update gatein flag 5, jika ada lanjut
			if(!property_exists($v,'bc_t_shipment'))
				{
					$update_data = array('istransfer'=>5);
					$this->tr_gatein_model
							->where('idkey',$v->idkey)
							->update($update_data);
				}elseif (strpos($v->hawb, 'i') !== false){ // cari hawb yg ada id kecil
					$update_data = array('hawb'=>strtoupper($v->hawb));
					$this->tr_gatein_model
							->where('idkey',$v->idkey)
							->update($update_data);
				}elseif ($v->status != 'ARR') {
					$update_data = array('istransfer'=>5);
					$this->tr_gatein_model
							->where('idkey',$v->idkey)
							->update($update_data);
				}
				else{
					$tracking_number = $v->hawb;
					$status = 'arrived';
					$status_date = $this->hongkong_date($v->tglscan);
					array_push($this->data_arrived, compact('tracking_number','status','status_date'));
				}
		}
		if(count($this->data_arrived) == 0)
		{
			echo "string";
			exit();
		}
		// buat body request
		$this->data = [
						'msg_type'=>'PACKAGE_STATUS',
						'package'=>$this->data_arrived
				  	  	];

				// kirim data ke API Lazada
				$lazadaRes = $this->send_lazada(json_encode($this->data));
				log_message('error','Arrived'.json_encode($lazadaRes));
				// // cek error respon lazada
				print_r($lazadaRes);

				$respon = $this->cek_error_respon($lazadaRes);
				log_message('all',$lazadaRes);
				if($respon !== true)
				{
					$request_time = $lazadaRes->response->request_time;
					$total_request = $lazadaRes->response->records->total;
					$new_request = $lazadaRes->response->records->new;
					$update_request = $lazadaRes->response->records->update;
					$id = $this->tr_status_send_model
							->insert(compact('request_time','total_request','new_request','update_request'));
					foreach ($this->data_arrived as $v) {
						$dataNya = ['id'=>$id,'hawb'=>$v['tracking_number'],'status'=>$v['status'],'time_host'=>$v['status_date']];
						$this->tr_pkg_status_model->insert($dataNya);
						// update gatein arrival jika sukses ke 1
						// flag tr gatein = 1 (sukses arrival)
						$this->tr_gatein_model->where('hawb',$v['tracking_number'])->update(['istransfer'=>1]);
					}
					// print_r($this->data_arrived);

					// $xx = $this->send_cloud_dbrespon($this->data_arrived,$request_time);
					// log_message('error',(string)$xx);
				}
	}
	// 
	
	public function send_lazada($data)
	{
		// // generate signatur for login
				$string_to_sign = urlencode('action='.$this->action.'&timestamp='.$this->date_time.'&userid='.$this->user_id);
				$signatur = $this->get_digest($string_to_sign, $this->key);
				$url_query = [
								'action'=>$this->action,
								'timestamp'=>$this->date_time,
								'userid'=>$this->user_id,
								'signature'=>$signatur
							 ];
				$url = $this->url.http_build_query($url_query);
		try{
			$response = $this->client->request('POST', $url,['body' => $data,'timeout' => 0],['debug' => true]);
					$stream = $response->getBody();
					$contents = $stream->getContents(); // returns all the contents
					log_message('error', $contents);
					return json_decode($contents);
					} catch (RequestException $e) {
					    	// echo Psr7\str($e->getRequest());
					    	log_message('error', $e->getRequest());
					    if ($e->hasResponse()) {
					        // echo Psr7\str($e->getResponse());
					        log_message('error', $e->getResponse());
					    }
					}
	}
	private function cek_error_respon($respon)
	{
		if((string)$respon->response->success === 'false' || (string)$respon->response->success === '')
			{
				// $a = (property_exists($lazadaRes->response,'error_type') ? $lazadaRes->response->error_type : 'Null');
				log_message('error', json_encode($respon));
				return true;
			}
	}
	private function get_digest ($data, $key) {
				return hash_hmac('sha256', $data, $key, false);
			}
	private function atom_date($date)
	{
		$datetime = new DateTime($date,new DateTimeZone("Asia/Hong_Kong"));
		return $datetime->format(DateTime::ATOM); // Updated ISO8601
	}
	private function hongkong_date($date)
	{
		$dt = new DateTime($date, new DateTimeZone("Asia/Hong_Kong"));
					$dt->modify("+1 hours");
					return $dt->format("Y-m-d H:i:s");
	}
	private function propertyExists($cls,$prt)
	{
		if(property_exists($cls,$prt) == true){ return $cls->$prt; }	
	}
	private function intervalDate($scan,$nowDate)
	{
		//set timezone to UTC to disregard daylight savings
		ini_set('date.timezone', 'Asia/Jakarta'); 
		$date1 = new \DateTime($scan);
		$date2 = new \DateTime($nowDate);
		$interval = new \DateInterval('PT1H');

		$periods = new \DatePeriod($date1, $interval, $date2);
		$hours = iterator_count($periods);
		return $hours;
	}
	public function send_cloud_dbrespon($data,$request_time)
	{
		try{
				$res = $this->client->request('POST', 'http://116.206.196.39:8080/api/lazadaNotif/arival',
						[
						    'form_params' => ['data'=>$data,'request_time'=>$request_time]
						]);
				return $res->getBody();

			} catch (RequestException $e) {
			    	echo Psr7\str($e->getRequest());
			    	log_message('error', $e->getRequest());
			    if ($e->hasResponse()) {
			        echo Psr7\str($e->getResponse());
			        log_message('error', $e->getResponse());
			    }
			}
	}
	
	function tes()
	{
		print_r($this->date_time);
		// result 
		// 2017-09-25T06:38:53-04:00
	}

}

/* End of file ClientLazada.php */
/* Location: ./application/controllers/ClientLazada.php */