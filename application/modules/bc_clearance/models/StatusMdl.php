<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
* 
*/
class StatusMdl extends CI_Model
{
	
	public $respon;

    public function __construct()
    {
        parent::__construct();
        //Do your magic here
        $this->load->helper('string');
    }
    public function insertTrackng($data,$table)
    {
          $this->db->limit(1);
          $this->db->where('NO_BARANG',$data->NO_BARANG);
          $this->db->where('KD_RESPON',$data->KD_RESPON);
          $this->db->select('id_res_cn');
          $x = $this->db->get($table);
          if(count($x->result()) == 0)
          {
            $this->db->insert($table,$data); 
            return $this->db->insert_id();
          }
    }
    public function get_hawb($data)
    {
       $this->db->where('hawb',$data);
       $this->db->select('hawb,tglawb,bc11,tglbc,nopos,subpos,Ident_kode_broker');
       $this->db->from('bc_t_shipment');
       $x = $this->db->get()->result();
       return $x;
    }

    public function get_datanya($tanggal, $respon_code)
    {
      // $qry_res = $this->db->query("call tarik_header('".$data."')");
     
      $qry_res = $this->db->query("call tarik_header('".$tanggal."',".$respon_code.")");
      $res = $qry_res->result();
      // $qry_res->next_result(); // Dump the extra resultset.
      $qry_res->free_result(); // Does what it says.
      return $res;
    }

    public function tes($data)
    {
     

     if (substr($data->HEADER->KD_RESPON,0,1) == '1') {
          $table = 'bc_respone_100';
     }
     elseif (substr($data->HEADER->KD_RESPON,0,1) == '2') {
          $table = 'bc_respone_200';
     }
      elseif (substr($data->HEADER->KD_RESPON,0,1) == '3') {
          $table = 'bc_respone_300';
     }
     elseif (substr($data->HEADER->KD_RESPON,0,1) == '4') {
          $table = 'bc_respone_400';
     }
     elseif (substr($data->HEADER->KD_RESPON,0,1) == '5') {
          $table = 'bc_respone_500';
     }
     elseif (substr($data->HEADER->KD_RESPON,0,1) == '9') {
          $table = 'bc_respone_900';
     }else{
          $table = 'bc_respone';
     }
     $NewPDF = cek_property($data->HEADER->PDF);
     if($NewPDF !== '')
     {
          $this->db->set('NewPDF',$NewPDF);
     }
          $this->db->insert($table,$data->HEADER);
      
  } // end fungsi

    public function updateFlagFielXml($data)
    {
      $this->db->where('id',$data);
      $this->db->update('bc_xml_file',['flag'=>1]);
    }
// insert nama filenya ke database
    public function insertFileName($data,$tgl,$statXML)
    {
          $this->db->limit(1);
          $this->db->where('nama_file',$data);
     $x = $this->db->get('bc_xml_file');
     if (count($x->result_array())== 0) {
        $xx = ['nama_file'=>$data,'tanggal'=>$tgl,'status_xml'=>$statXML];
        $this->db->insert('bc_xml_file',$xx);
      } 
    }
    public function getFileXml()
    {
                 $this->db->where('flag','0');
      $dataNya = $this->db->get('bc_xml_file', 100);
      return $dataNya->result();
    }
    public function getRespon($data)
    {
        if (substr($data->HEADER->KD_RESPON,0,1) == '1') {
              $this->db->where('NO_BARANG', $data->HEADER->NO_BARANG);
              $this->db->where('KD_RESPON', $data->HEADER->KD_RESPON);
              $this->db->get('bc_respone_100');
              $x = $this->db->affected_rows();
              if ($x == 0) {
                  $this->db->set($data->HEADER);
                  $this->db->insert('bc_respone_100');
              }
         }
         elseif (substr($data->HEADER->KD_RESPON,0,1) == '2') {
              $this->db->where('NO_BARANG', $data->HEADER->NO_BARANG);
              $this->db->where('KD_RESPON', $data->HEADER->KD_RESPON);
              $this->db->get('bc_respone_200');
              $x = $this->db->affected_rows();
              if ($x == 0) {
                  $this->db->set($data->HEADER);
                  $this->db->insert('bc_respone_200');
              }
         }
          elseif (substr($data->HEADER->KD_RESPON,0,1) == '3') {
              $this->db->where('NO_BARANG', $data->HEADER->NO_BARANG);
              $this->db->where('KD_RESPON', $data->HEADER->KD_RESPON);
              $this->db->get('bc_respone_300');
              $x = $this->db->affected_rows();
              if ($x == 0) {
                  $this->db->set($data->HEADER);
                  $this->db->insert('bc_respone_300');
              }
         }
         elseif (substr($data->HEADER->KD_RESPON,0,1) == '4') {
              $this->db->where('NO_BARANG', $data->HEADER->NO_BARANG);
              $this->db->where('KD_RESPON', $data->HEADER->KD_RESPON);
              $this->db->get('bc_respone_400');
              $x = $this->db->affected_rows();
              if ($x == 0) {
                  $this->db->set($data->HEADER);
                  $this->db->insert('bc_respone_400');
              }
         }
         elseif (substr($data->HEADER->KD_RESPON,0,1) == '5') {
              $this->db->where('NO_BARANG', $data->HEADER->NO_BARANG);
              $this->db->where('KD_RESPON', $data->HEADER->KD_RESPON);
              $this->db->get('bc_respone_500');
              $x = $this->db->affected_rows();
              if ($x == 0) {
                  $this->db->set($data->HEADER);
                  $this->db->insert('bc_respone_500');
              }
         }
         elseif (substr($data->HEADER->KD_RESPON,0,1) == '9') {
              $this->db->where('NO_BARANG', $data->HEADER->NO_BARANG);
              $this->db->where('KD_RESPON', $data->HEADER->KD_RESPON);
              $this->db->get('bc_respone_900');
              $x = $this->db->affected_rows();
              if ($x == 0) {
                  $this->db->set($data->HEADER);
                  $this->db->insert('bc_respone_900');
              }
         }else{
              $this->db->where('NO_BARANG', $data->HEADER->NO_BARANG);
              $this->db->where('KD_RESPON', $data->HEADER->KD_RESPON);
              $this->db->get('bc_respone');
              $x = $this->db->affected_rows();
              if ($x == 0) {
                  $this->db->set($data->HEADER);
                  $this->db->insert('bc_respone');
              }
         }
    }

}