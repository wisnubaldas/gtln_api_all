<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Gtln_mdl extends CI_Model {
    public $GTLN;
    public function __construct()
    {
        parent::__construct();
        //Do your magic here
        // $this->GTLN = $this->load->database('GTLN', TRUE);
        // $this->load->library('mongol');
        $this->load->helper('string');
    }
    // start model baru
    function get_bc11()
    {
        $this->db->where('flag_bc11',1);
        $this->db->select('hawb,tglawb,bc11,tglbc,nopos,subpos');
        $this->db->limit(100);
        $result = $this->db->get('bc_t_shipment');
        return $result->result(); 
    }
    function insert_bc11($data)
    {
       
        $xml = xml2array(simplexml_load_string($data));
                $this->db->insert_batch('bc_respone_bc11',simplexml_load_string($data));
                // print_r($this->db->last_query());
                $x = array(
                    'flag_bc11' => 2
                );
                $this->db->where_in('hawb', $xml['NO_BARANG']);
                $this->db->update('bc_t_shipment', $x);
    }
    function get_shipment()
    {
        $this->db->where('flag_xml',1);
        $this->db->select('Id_aju,kdkntr,FlightName,flightno,negaraasal,Destination,invoice,
                            tglInvo,Package,bc11,tglbc,nopos,subpos,mawb,tglawb,hawb,tglawb,Origin,
                            nmkirim,almkirim,consignee_NPWP,consignee_name,consignee_address,consignee_phone,
                            broker_name,broker_address,broker_ijin,currency_valuta,FOB,TotalTax,insurance,FreightCost,
                            CIF,weight_original,Weight,ndpbm,bm_value,pph_value,ppn_value,ppnBM_value,hsCode,kindofGood,idterima,No_idterima,
                            bm_persen,pph_persen,ppn_persen,ppnBM_persen,broker_ijin_date,Ident_kode_broker');
        $this->db->limit(200);
        $result = $this->db->get('bc_t_shipment');
        return $result->result();
    }
    function cek_xmlnya($hawb)
    {
        $this->db->where('hawb',$hawb);
        $this->db->select('Id_aju,kdkntr,FlightName,flightno,negaraasal,Destination,invoice,
                            tglInvo,Package,bc11,tglbc,nopos,subpos,mawb,tglawb,hawb,tglawb,Origin,
                            nmkirim,almkirim,consignee_NPWP,consignee_name,consignee_address,consignee_phone,
                            broker_name,broker_address,broker_ijin,currency_valuta,FOB,TotalTax,insurance,FreightCost,
                            CIF,weight_original,Weight,ndpbm,bm_value,pph_value,ppn_value,ppnBM_value,hsCode,kindofGood,idterima,No_idterima,
                             bm_persen,pph_persen,ppn_persen,ppnBM_persen,broker_ijin_date,Ident_kode_broker');
        $result = $this->db->get('bc_t_shipment');
        return $result->result();
    }

   public function err_response($data,$no_barang)
    {
        $arr =  $this->xml2array($data);
        $arr += ['NO_BARANG'=>$no_barang];
        $this->db->insert('bc_respone_ERR',$arr);
            // return $this->CAS->last_query();
    }    
     public function update_flag($host,$flag)
    {
            // / update 
            // print_r($host);
            $data = array(
                'flag_xml' => $flag
            );
                $this->db->where('hawb', $host);
                $this->db->update('bc_t_shipment', $data);
                // return $this->CAS->last_query();
    }

    public function send_data_respon($data)
    {
        if (substr($data->KD_RESPON,0,1) == '1')
        {
            $this->db->insert('bc_respone_100',$data);
            return $this->db->last_query();
        }elseif (substr($data->KD_RESPON,0,1) == '2')
        {
            $this->db->insert('bc_respone_200',$data);
            return $this->db->last_query();
        }elseif(substr($data->KD_RESPON,0,1) == '3')
        {
            $this->db->insert('bc_respone_300',$data);
            return $this->db->last_query();
        }elseif (substr($data->KD_RESPON,0,1) == '4')
        {
            $this->db->insert('bc_respone_400',$data);
            return $this->db->last_query();
        }elseif (substr($data->KD_RESPON,0,1) == '5')
        {
            $this->db->insert('bc_respone_500',$data);
            return $this->db->last_query();
        }elseif (substr($data->KD_RESPON,0,1) == '9')
        {
            $this->db->insert('bc_respone_900',$data);
            return $this->db->last_query();
        }else{
            $this->db->insert('bc_respone',$data);
            return $this->db->last_query();
        }
    }
    
public function getallrespon_res($data)
    {
        // insert response
        foreach ($data as $value) {
            $NO_BARANG = cek_property($value->HEADER->NO_BARANG);
            $TGL_HOUSE_BLAWB = cek_property($value->HEADER->TGL_HOUSE_BLAWB);
            $KD_RESPON = cek_property($value->HEADER->KD_RESPON);
            $KET_RESPON = cek_property($value->HEADER->KET_RESPON);
            $WK_REKAM = cek_property($value->HEADER->WK_REKAM);
            $KODE_BILLING = cek_property($value->HEADER->KODE_BILLING);
            $TOTAL_BILLING = cek_property($value->HEADER->TOTAL_BILLING);
            $TGL_JT_TEMPO = cek_property($value->HEADER->TGL_JT_TEMPO);
            $TGL_BILLING = cek_property($value->HEADER->TGL_BILLING);
            $KD_DOK_BILLING = cek_property($value->HEADER->KD_DOK_BILLING);
            $NO_SPPB = cek_property($value->HEADER->NO_SPPB);
            $TGL_SPPB = cek_property($value->HEADER->TGL_SPPB);
            $NO_SPPBMCP = cek_property($value->HEADER->NO_SPPBMCP);
            $TGL_SPPBMCP = cek_property($value->HEADER->TGL_SPPBMCP);
            $NO_SPTNP = cek_property($value->HEADER->NO_SPTNP);
            $TGL_SPTNP = cek_property($value->HEADER->TGL_SPTNP);
            $NILAI_SPTNP = cek_property($value->HEADER->NILAI_SPTNP);
            $KD_KANTOR = cek_property($value->HEADER->KD_KANTOR);
            $NDPBM = cek_property($value->HEADER->NDPBM);
            $NIL_PAB_RP = cek_property($value->HEADER->NIL_PAB_RP);
            $NO_PIBK = cek_property($value->HEADER->NO_PIBK);
            $TGL_PIBK = cek_property($value->HEADER->TGL_PIBK);
            $NO_SPBL = cek_property($value->HEADER->NO_SPBL);
            $TGL_SPBL = cek_property($value->HEADER->TGL_SPBL);

            $NewPDF = '';
            $this->db->trans_start();

            if(isset($value->HEADER->PDF))
            {
                $NewPDF = $value->HEADER->PDF;
                $PDF_file = write_file('./asset/xmlfile/pdfFile/'.date('ymdHis').'_'.$NO_BARANG.'.pdf', base64_decode($NewPDF));
            }
            // $PDF = base64_decode($NewPDF);

            if (substr($KD_RESPON,0,1) == '1')
            {
                $this->db->insert('bc_respone_100',compact('NO_BARANG','TGL_HOUSE_BLAWB','KD_RESPON','KET_RESPON','WK_REKAM','NO_SPPBMCP','TGL_SPPBMCP','TGL_SPPB','NO_SPPB','TGL_SPTNP','NO_SPTNP', 'NILAI_SPTNP ','TOTAL_BILLING','KODE_BILLING','TGL_JT_TEMPO','TGL_BILLING','KD_DOK_BILLING','TGL_SPBL','NO_SPBL','TGL_SPBL','NO_SPBL','TGL_PIBK', 'NO_PIBK', 'NIL_PAB_RP', 'NDPBM','KD_KANTOR','NewPDF'));
            }elseif (substr($KD_RESPON,0,1) == '2')
            {
                $this->db->insert('bc_respone_200',compact('NO_BARANG','TGL_HOUSE_BLAWB','KD_RESPON','KET_RESPON','WK_REKAM','NO_SPPBMCP','TGL_SPPBMCP','TGL_SPPB','NO_SPPB','TGL_SPTNP','NO_SPTNP', 'NILAI_SPTNP ','TOTAL_BILLING','KODE_BILLING','TGL_JT_TEMPO','TGL_BILLING','KD_DOK_BILLING','TGL_SPBL','NO_SPBL','TGL_SPBL','NO_SPBL','TGL_PIBK', 'NO_PIBK', 'NIL_PAB_RP', 'NDPBM','KD_KANTOR','NewPDF'));

            }elseif(substr($KD_RESPON,0,1) == '3')
            {
                $this->db->insert('bc_respone_300',compact('NO_BARANG','TGL_HOUSE_BLAWB','KD_RESPON','KET_RESPON','WK_REKAM','NO_SPPBMCP','TGL_SPPBMCP','TGL_SPPB','NO_SPPB','TGL_SPTNP','NO_SPTNP', 'NILAI_SPTNP ','TOTAL_BILLING','KODE_BILLING','TGL_JT_TEMPO','TGL_BILLING','KD_DOK_BILLING','TGL_SPBL','NO_SPBL','TGL_SPBL','NO_SPBL','TGL_PIBK', 'NO_PIBK', 'NIL_PAB_RP', 'NDPBM','KD_KANTOR','NewPDF'));

            }elseif (substr($KD_RESPON,0,1) == '4')
            {
                $this->db->insert('bc_respone_400',compact('NO_BARANG','TGL_HOUSE_BLAWB','KD_RESPON','KET_RESPON','WK_REKAM','NO_SPPBMCP','TGL_SPPBMCP','TGL_SPPB','NO_SPPB','TGL_SPTNP','NO_SPTNP', 'NILAI_SPTNP ','TOTAL_BILLING','KODE_BILLING','TGL_JT_TEMPO','TGL_BILLING','KD_DOK_BILLING','TGL_SPBL','NO_SPBL','TGL_SPBL','NO_SPBL','TGL_PIBK', 'NO_PIBK', 'NIL_PAB_RP', 'NDPBM','KD_KANTOR','NewPDF'));

            }elseif (substr($KD_RESPON,0,1) == '5')
            {
                $this->db->insert('bc_respone_500',compact('NO_BARANG','TGL_HOUSE_BLAWB','KD_RESPON','KET_RESPON','WK_REKAM','NO_SPPBMCP','TGL_SPPBMCP','TGL_SPPB','NO_SPPB','TGL_SPTNP','NO_SPTNP', 'NILAI_SPTNP ','TOTAL_BILLING','KODE_BILLING','TGL_JT_TEMPO','TGL_BILLING','KD_DOK_BILLING','TGL_SPBL','NO_SPBL','TGL_SPBL','NO_SPBL','TGL_PIBK', 'NO_PIBK', 'NIL_PAB_RP', 'NDPBM','KD_KANTOR','NewPDF'));

            }elseif (substr($KD_RESPON,0,1) == '9')
            {
                $this->db->insert('bc_respone_900',compact('NO_BARANG','TGL_HOUSE_BLAWB','KD_RESPON','KET_RESPON','WK_REKAM','NO_SPPBMCP','TGL_SPPBMCP','TGL_SPPB','NO_SPPB','TGL_SPTNP','NO_SPTNP', 'NILAI_SPTNP ','TOTAL_BILLING','KODE_BILLING','TGL_JT_TEMPO','TGL_BILLING','KD_DOK_BILLING','TGL_SPBL','NO_SPBL','TGL_SPBL','NO_SPBL','TGL_PIBK', 'NO_PIBK', 'NIL_PAB_RP', 'NDPBM','KD_KANTOR','NewPDF'));

            }else{
                $this->db->insert('bc_respone',compact('NO_BARANG','TGL_HOUSE_BLAWB','KD_RESPON','KET_RESPON','WK_REKAM','NO_SPPBMCP','TGL_SPPBMCP','TGL_SPPB','NO_SPPB','TGL_SPTNP','NO_SPTNP', 'NILAI_SPTNP ','TOTAL_BILLING','KODE_BILLING','TGL_JT_TEMPO','TGL_BILLING','KD_DOK_BILLING','TGL_SPBL','NO_SPBL','TGL_SPBL','NO_SPBL','TGL_PIBK', 'NO_PIBK', 'NIL_PAB_RP', 'NDPBM','KD_KANTOR','NewPDF'));
            }

            if (isset($value->HEADER->DETIL_PUNGUTAN)) {
                foreach ( $value->HEADER->DETIL_PUNGUTAN->PUNGUTAN as $v) {
                    $id_res = $NO_BARANG;
                    $KD_PUNGUTAN = $v->KD_PUNGUTAN;
                    $NILAI = $v->NILAI;
                    $this->db->insert('bc_res_lartas',compact('id_res','KD_PUNGUTAN','NILAI'));
                }
            }
         $this->db->trans_complete();
        }        
    }

    public function get_hawb($data)
    {
           $this->db->where_in('NO_BARANG',$data);
           $this->db->select('NO_BARANG,TGL_HOUSE_BLAWB,NO_BC11,TGL_BC11,NO_POS_BC11,NO_SUBPOS_BC11');
           $q = $this->db->from('bc_header');
           $xx = $q->get()->result();
           return $xx;
    }

    public function insert_cekstatus($data,$host)
    {
      
      if ($data->HEADER->KD_RESPON == 'ERR') {
            $NO_BARANG = $host;
            $KD_RESPON = $data->HEADER->KD_RESPON;
            $WK_RESPON = $data->HEADER->WK_RESPON;
            $KET_RESPON = $data->HEADER->KET_RESPON;
          $this->db->insert('bc_respone_ERR',compact('NO_BARANG','KD_RESPON','WK_RESPON','KET_RESPON'));
          $this->idrespon = $this->db->insert_id();
      }else{
            if(isset($data->HEADER->PDF))
        {
          $PDF_file = write_file('./asset/xmlfile/pdfFile/'.date('ymdHis').'_'.$data->HEADER->NO_BARANG.'.pdf', base64_decode($data->HEADER->PDF));
        }  

      $cek = $this->db->get_where('bc_respone', array('NO_BARANG' => $data->HEADER->NO_BARANG,'KD_RESPON' => $data->HEADER->KD_RESPON))->result();
      if(count($cek) == '')
      {
             $this->db->set('NewPDF',cek_property($data->HEADER->PDF));
              $this->db->insert('bc_respone',$data);
              $this->idrespon = $this->db->insert_id();
              return $this->db->last_query();

      }else{
            $NewPDF = cek_property($data->HEADER->PDF);
            $this->db->where('NO_BARANG', $data->HEADER->NO_BARANG);
            $this->db->where('KD_RESPON', $data->HEADER->KD_RESPON);
            $this->db->set('NewPDF',$NewPDF);
            $this->db->update('bc_respone', $data->HEADER);
            }
      }
      
      
  } // end fungsi
    function xml2array ( $xmlObject, $out = array () )
    {
        foreach ( (array) $xmlObject as $index => $node )
            $out[$index] = ( is_object ( $node ) ) ? xml2array ( $node ) : $node;
        return $out;
    }
}

/* Pibk_mdl of file ModelName.php */