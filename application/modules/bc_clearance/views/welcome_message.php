<!DOCTYPE html>
<html>
<head>
	<title>Log message</title>
	<META HTTP-EQUIV="refresh" CONTENT="120">
</head>
<body>
	<h3><?php echo $title; ?> GTLN</h3>
	<hr>
    <a href="http://192.168.88.22:8080/cekstatus/cekulangXml">Cek Ulang XML</a>
    <a href="http://192.168.88.22:8080/api_tlc/cekstatus?hawb=">Cek Status Respon</a>
    <a href="http://192.168.88.22:8080/cekstatus/serviceTracking?hawb=">Cek Status Tracking</a>

    <span><?php if(isset($error_warning) == true){echo $error_warning;} ?></span>
	<div id="content">
    <div class="container-fluid">
        <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title"><i class="fa fa-exclamation-triangle"></i></h3>
        </div>
        <div class="panel-body">
            <textarea wrap="off" rows="30" cols="150" readonly class="form-control"><?php echo $log; ?></textarea>
        </div>
        </div>
    </div>
</div>
<script type="text/javascript">    
    setInterval(page_refresh, 1*60000); //NOTE: period is passed in milliseconds
</script>
</body>
</html>