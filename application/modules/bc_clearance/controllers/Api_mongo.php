<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Api_mongo extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		
		$this->load->model('Tlcmongo_mdl');
		$this->load->library('mongol');

		// print_r($x);
	}

	public function index()
	{
		
	}
	public function post_data()
	{
		$this->filenya = file_get_contents('./asset/wsdl/all_response/170519_05190000110622.xml');
		$xmlparse = new SimpleXMLElement($this->filenya);
		$data;
		foreach ($xmlparse->RESPONSE as $value) {
			$x = $this->mongol->insert_one('respon',$value->HEADER);
			$data[] = $x;
		}
		print_r($data);
	}
	function post_many_data()
	{
		
           
		$data = [
			[
	           'username' => 'admin',
	           'email' => 'admin@example.com',
	           'name' => 'Admin User',
	       ],
	       [
	           'username' => 'test',
	           'email' => 'test@example.com',
	           'name' => 'Test User',
	       ],
   		];
		$x = $this->mongol->insert_many('menyeng',$data);
		print_r($x);
	}
	function get_data()
	{
		if($this->input->get('statuscode'))
		{
			// $data = ['KD_RESPON'=>$this->input->get('statuscode')];
			// $this->mongol->select('naam,dasdasd,dasdasd,dasasd');
			$x = $this->mongol->get();
			print_r($x);

		}else{
			$x = $this->mongol->select('respon');
			print_r($x);
		}
	}

}

/* End of file Api_mongo.php */
/* Location: ./application/controllers/Api_mongo.php */