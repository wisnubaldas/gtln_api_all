<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class PecahBK extends MX_Controller {
	function __construct ()
    {
        parent::__construct();
        $this->client = new \GuzzleHttp\Client(['cookies' => true]);
        $this->load->library('mongo_db');
		$this->mongo_db->reconnect([
					    'config' => [
					        'connection' => [
					            'host' => ['localhost'],
					            'port' => [],
					            'user_name' => '',
    							'user_password' => '',
					            'db_name' => 'db_log_barangkiriman',
					        ]
					    ]
					]);

        date_default_timezone_set('Asia/Jakarta');
        // Load XML writer library
        $this->load->library('MY_xml_writer');
        $model = ['pecahModel'];
        $this->load->model($model);
        $this->load->helper('string');
    }

public function getShipment()
{
	$itung = $this->pecahModel->on('GTLN')->where('flag_xml',1)->as_array()->count_rows();
	if($itung != 0)
	{

        try {
            $return = $this->pecahModel
                ->on('GTLN')
                ->fields('Id_aju,kdkntr,FlightName,flightno,negaraasal,Destination,invoice,tglInvo,Package,bc11,tglbc,nopos,subpos,subsubpos,mawb,tglawb,hawb,tglawb,Origin,nmkirim,almkirim,consignee_NPWP,consignee_name,consignee_address,consignee_phone,broker_name,broker_address,broker_ijin,currency_valuta,FOB,TotalTax,insurance,FreightCost,CIF,weight_original,Weight,ndpbm,bm_value,pph_value,ppn_value,ppnBM_value,hsCode,kindofGood,idterima,No_idterima,bm_persen,pph_persen,ppn_persen,ppnBM_persen,broker_ijin_date,Ident_kode_broker')
                ->where('flag_xml',1)
                ->limit(3)
                ->as_array()
                ->get_all();
            print_r($return);
            
            $r = $this->client->request('POST', 'http://116.206.197.1/gtln1/api', ['json' => $return]);
            $dataID = $r->getBody();
            echo $dataID;

        }catch (Exception $ex) {
            echo $ex->getResponse()->getBody();
            echo $ex->getResponse()->getStatusCode();
        }
	}else {
		echo 'brek';
		exit();
	}


			
			

}
public function getResponCloud()
{
	try {
	    $r = $this->client
	    			->request('GET', 'http://116.206.197.1/gtln1/api');
					$dataID = (string)$r->getBody();
					echo $dataID;

	        }catch (Exception $ex) {
		            echo $ex->getResponse()->getBody();
		            echo $ex->getResponse()->getStatusCode();
	        }

	// $client     = new GuzzleHttp\Client();
	// 		// web001
	// 		$url = 'http://116.206.196.101/gtln_bc/api';
	// 		$response = $client->request('GET', $url);
	// 		$body = $response->getBody();
			
	// 		$data = json_decode($body);
	// 		if(count($data) != 0)
	// 		{
	// 			foreach ($data as $v) {
	// 			$this->mongo_db->insert('tmp_bc_response',(array)$v);
	// 			}
	// 		print_r($data);
	// 		}

}


} // end class

/* End of file PecahBK.php */
/* Location: ./application/modules/bc_clearance/controllers/PecahBK.php */