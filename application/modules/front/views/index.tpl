<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Favicon-->
        <link rel="icon" href="favicon.ico" type="image/x-icon">
        <!-- Google Fonts -->
        {* <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css"> *}
        <link href="{$this->parser->theme_url('css/material_font/material-icons.css')}" rel="stylesheet" type="text/css">

        <title>{block name=title}{$title}{/block} GTLN</title>
        
        <link href="{$this->parser->theme_url('plugins/bootstrap/css/bootstrap.css')}" rel="stylesheet">
        <link href="{$this->parser->theme_url('plugins/node-waves/waves.css')}" rel="stylesheet">
        <link href="{$this->parser->theme_url('plugins/animate-css/animate.css')}" rel="stylesheet">

        {foreach from=$css_plugin key=k item=v}
          <link href="{$v}" rel="stylesheet">
        {/foreach}
        {foreach from=$css key=k item=v}
        {css({$v})}
        {/foreach}
        <link href="{$this->parser->theme_url('css/themes/all-themes.css')}" rel="stylesheet" type="text/css">
        
    </head>
    <body class="{$body_class}">
        {block name=body}
            {$body}
        {/block}
        <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-red">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p>Please wait...</p>
        </div>
    </div>
    <script src="{$this->parser->theme_url('plugins/jquery/jquery.min.js')}"></script>
    <script src="{$this->parser->theme_url('plugins/bootstrap/js/bootstrap.js')}"></script>
    <script src="{$this->parser->theme_url('plugins/node-waves/waves.js')}"></script>
   {foreach from=$js_plugin key=k item=v}
    <script src="{$v}"></script>
    {/foreach}
    {foreach from=$js key=k item=v}
    {js({$v})}
    {/foreach}
        

    </body>
</html>
