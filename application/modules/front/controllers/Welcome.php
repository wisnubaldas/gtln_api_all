<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends MX_Controller {

	public $data = [];
	public function __construct()
	{
		parent::__construct();
		$this->load->library('parser');
		$this->load->library('ion_auth');
	}
	public function index()
	{	

		if (!$this->ion_auth->logged_in())
		{
		
		$this->data['title'] = "Login Pages";
		$this->data['css'] = ['materialize.css','style.css'];
		$this->data['body_class'] = 'login-page';
		$this->data['js_plugin'] = [$this->parser->theme_url('plugins/jquery-validation/jquery.validate.js')];
		$this->data['js'] = ['admin.js','pages/examples/sign-in.js'];
		$this->parser->parse('auth/login.tpl',$this->data);

		}else {
			$this->data['title'] = "Response BC";
			$this->data['css'] = ['materialize.css','style.css'];
			$this->data['js'] = ['admin.js','pages/charts/flot.js','demo.js'];
			$this->data['js_plugin'] = [
				$this->parser->theme_url('plugins/jquery-slimscroll/jquery.slimscroll.js'),
				$this->parser->theme_url('plugins/flot-charts/jquery.flot.js'),
				$this->parser->theme_url('plugins/flot-charts/jquery.flot.resize.js'),
				$this->parser->theme_url('plugins/flot-charts/jquery.flot.pie.js'),
				$this->parser->theme_url('plugins/flot-charts/jquery.flot.categories.js'),
				$this->parser->theme_url('plugins/flot-charts/jquery.flot.time.js'),
			];
			$this->data['users'] = $this->ion_auth->user()->row();
			$this->parser->parse('respon/home_body.tpl',$this->data);
		}
		
	}
	public function login()
	{
		$identity = $this->input->post('username');
		$password = $this->input->post('password');
		$remember = FALSE; // remember the user
		if (!$this->input->is_ajax_request()) {
		   $this->output->set_status_header(403);
		   // exit('No direct script access allowed');
		}
		if ($this->input->post('rememberme') == 'on') {
			$remember = TRUE;
		};
		$data = $this->ion_auth->login($identity, $password, $remember);
		if(!$data)
		{
			// $this->output
		        // ->set_content_type('application/json')
		        // ->set_output(json_encode(array('data'=>false)));
		         $this->output->set_status_header(403);	
		}else{
			$this->data['title'] = "Response BC";
			$this->data['css'] = ['materialize.css','style.css'];
			$this->data['js'] = ['admin.js','pages/charts/flot.js','demo.js'];
			$this->data['js_plugin'] = [
				$this->parser->theme_url('plugins/jquery-slimscroll/jquery.slimscroll.js'),
				$this->parser->theme_url('plugins/flot-charts/jquery.flot.js'),
				$this->parser->theme_url('plugins/flot-charts/jquery.flot.resize.js'),
				$this->parser->theme_url('plugins/flot-charts/jquery.flot.pie.js'),
				$this->parser->theme_url('plugins/flot-charts/jquery.flot.categories.js'),
				$this->parser->theme_url('plugins/flot-charts/jquery.flot.time.js'),
			];
			$this->data['users'] = $this->ion_auth->user()->row();
			$this->parser->parse('respon/home_body.tpl',$this->data);
		}
	}
	// public function home()
	// {
	// 			$data['js'] = [
	// 					'admin.js',
	// 					'pages/index.js',
	// 					'demo.js'
	// 					];
	// 	$data['js_plugin'] = [
	// 							$this->parser->theme_url('plugins/bootstrap-select/js/bootstrap-select.js'),
	// 							$this->parser->theme_url('plugins/jquery-slimscroll/jquery.slimscroll.js'),
	// 							$this->parser->theme_url('plugins/jquery-countto/jquery.countTo.js'),
	// 							$this->parser->theme_url('plugins/raphael/raphael.min.js'),
	// 							$this->parser->theme_url('plugins/morrisjs/morris.js'),
	// 							$this->parser->theme_url('plugins/chartjs/Chart.bundle.js'),
	// 							$this->parser->theme_url('plugins/flot-charts/jquery.flot.js'),
	// 							$this->parser->theme_url('plugins/flot-charts/jquery.flot.resize.js'),
	// 							$this->parser->theme_url('plugins/flot-charts/jquery.flot.pie.js'),
	// 							$this->parser->theme_url('plugins/flot-charts/jquery.flot.categories.js'),
	// 							$this->parser->theme_url('plugins/flot-charts/jquery.flot.time.js'),
	// 							$this->parser->theme_url('plugins/jquery-sparkline/jquery.sparkline.js'),
	// 							];
	// 	$data['css_plugin'] = [
	// 							$this->parser->theme_url('plugins/morrisjs/morris.css'),
	// 							];
	// 	$data['css'] = ['style.css'];
 //        $data['title'] = "API";
 //        // Load the template from the views directory
 //        $this->parser->parse("index.tpl", $data);
	// }
}
