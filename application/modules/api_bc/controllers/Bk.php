<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bk extends REST_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->library('mongo_db');
		$this->mongo_db->reconnect([
					    'config' => [
					        'connection' => [
					            'host' => ['10.0.0.29'],
					            'port' => [],
					            'user_name' => '',
    							'user_password' => '',
					            'db_name' => 'db_barangkiriman',
					        ]
					    ]
					]);
	}
	
	public function index_post()
	{
	    $data = $this->post();
	    if(count($data) == 0)
        {
            $this->response('data kosong', 500);
        }else{
            $insertId = $this->mongo_db->insertAll('tmp_xml_bc_shipment',$data);
            $this->_log('success','insert ke tmp_xml_bc_shipment '.implode($insertId));
            $this->response($insertId, 200);
        }

	}
	public function index_get()
	{
		$result = $this->mongo_db->where('flag','default')->limit(200)->get('tmp_bc_response');
		$resp = [];
		if($result)
		{
			foreach ($result as $v) {
				$this->mongo_db->set('flag','tarik')->where('_id',new MongoDB\BSON\ObjectID($v['_id']));
				$update = $this->mongo_db->update('tmp_bc_response');
				if($update)
				{
					$v = array_diff_key($v, ['_id'=>$v['_id'],'flag' => "tarik", "idnya" => $v['idnya']]);
					array_push($resp,array_merge($v,['flag' => "default"]));
				}
			}
			$this->_log('success','tarik respon tmp_bc_response ke server gtln ');
			$this->response($resp, 200);
		}
//        return $this->_cariDelete();
	}
	private  function _cariDelete()
    {

        $collection = (new MongoDB\Client( "mongodb://10.0.0.29" ))->db_barangkiriman->log_feeder;
        $deletedRestaurant = $collection->findOneAndDelete(
            [ '_id' => new MongoDB\BSON\ObjectID('5a4200cda2df3103ee0128c1')]
        );
        foreach($deletedRestaurant as $r){
            print_r($r);
        }

    }
	private function _log($log,$message)
	{
		$this->mongo_db->insert('log_bc',['log'=>$log,'message'=>$message,'date'=>strtotime('now')]);
	}
	public function tarikRespon_get()
	{
		$this->response($this->get(), 200);	
	}

}

/* End of file Bk.php */
/* Location: ./application/modules/api_bc/controllers/Bk.php */