<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Posid extends MX_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('posModel');
		$this->wsdl = APPPATH.'modules/posid/WSPosDev_2015_12_02.wsdl.xml';
		$this->setting = array(
        		"exceptions" => true,        
                'stream_context'=> stream_context_create(array('ssl'=> array(
                    'verify_peer'=>false,
                    'verify_peer_name'=>false, 
                    'allow_self_signed' => true 
                        )
                    )
                )
            );
	}
	public function index()
	{
		echo 'string';
	}
	public function addPosting()
	{
		try {
			$this->client = new SoapClient($this->wsdl, $this->setting);
			$data = $this->posModel->addPosting();
		$this->respon_bc = $this->client->__soapCall("addPosting",["addPosting"=>$data]);
		print_r($this->respon_bc);

		}catch (Exception $exception) {
				echo $exception;
		}
	}
	public function addPickup()
	{
		try {
			$this->client = new SoapClient($this->wsdl, $this->setting);
			$data = $this->posModel->addPickup();
		$this->respon_bc = $this->client->__soapCall("addPickup",["addPickup"=>$data]);
		print_r($this->respon_bc);

		}catch (Exception $exception) {
				echo $exception;
		}
	}
	public function getTrackAndTrace()
	{
		try {
			$this->client = new SoapClient($this->wsdl, $this->setting);
			$data = $this->posModel->getTrackAndTrace();
		$this->respon_bc = $this->client->__soapCall("getTrackAndTrace",["getTrackAndTrace"=>$data]);
		print_r($this->respon_bc);

		}catch (Exception $exception) {
				echo $exception;
		}
	}
	public function getPosCodeByAddrAndCity()
	{
		try {
			$this->client = new SoapClient($this->wsdl, $this->setting);
			$data = $this->posModel->getPosCodeByAddrAndCity();
		$this->respon_bc = $this->client->__soapCall("getPosCodeByAddrAndCity",["getPosCodeByAddrAndCity"=>$data]);
		print_r($this->respon_bc);

		}catch (Exception $exception) {
				echo $exception;
		}
	}

}

/* End of file Posid.php */
/* Location: ./application/modules/posid/controllers/Posid.php */