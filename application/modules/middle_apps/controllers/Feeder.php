<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Feeder extends MX_Controller {

private $url = 'https://crossborder.lazada.com/api/3pl/v2/?'; // production lazada
private $user_id = 'GTLN';
private $key = '0NEOFNLDFJDK1';
private $action = 'StatusUpdate';
private $log_respon;
public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Macau');
		$this->mongo_db->connect();
		$this->client = new \GuzzleHttp\Client(['cookies' => true]);
		$this->date_time = $this->atom_date(date('Y-m-d H:i:s'));
		$this->load->library('mongo_db');
		$this->mongo_db->reconnect([
					    'config' => [
					        'connection' => [
					            'host' => ['10.0.0.29'],
					            'port' => [],
					            'user_name' => '',
    							'user_password' => '',
					            'db_name' => 'tracking_lazada',
					        ]
					    ]
					]);		
		
	}
private function atom_date($date)
	{
		$datetime = new DateTime($date,new DateTimeZone("Asia/Hong_Kong"));
		return $datetime->format(DateTime::ATOM); // Updated ISO8601
	}
private function get_digest ($data, $key) {
				return hash_hmac('sha256', $data, $key, false);
	}
private function send_lazada($data)
	{
		// // generate signatur for login
				$string_to_sign = urlencode('action='.$this->action.'&timestamp='.$this->date_time.'&userid='.$this->user_id);
				$signatur = $this->get_digest($string_to_sign, $this->key);
				$url_query = [
								'action'=>$this->action,
								'timestamp'=>$this->date_time,
								'userid'=>$this->user_id,
								'signature'=>$signatur
							 ];
				$url = $this->url.http_build_query($url_query);
		try{
			$response = $this->client->request('POST', $url,['body' => json_encode($data),'timeout' => 0],['debug' => true]);
			$stream = $response->getBody();
			$contents = $stream->getContents(); // returns all the contents
				return json_decode($contents);
					} catch (RequestException $e) {
					    	// echo Psr7\str($e->getRequest());
					    	log_message('error', $e->getRequest());
					    if ($e->hasResponse()) {
					        // echo Psr7\str($e->getResponse());
					        // echo $e->getResponse();
					        log_message('error', $e->getResponse());
					    }
					}
	}
private function __cekhostname()
{
	$host = gethostname();
		// echo $host;
		if($host == 'app008')
		{
			$fedder = 'fd01';			
		}elseif ($host == 'app009') {
			$fedder = 'fd02';
		}elseif ($host == 'app010') {
			$fedder = 'fd03';
		}elseif ($host == 'app011') {
			$fedder = 'fd04';
		}elseif ($host == 'app014') {
			$fedder = 'fd05';
		}elseif ($host == 'app015') {
			$fedder = 'fd06';
		}elseif ($host == 'app016') {
			$fedder = 'fd07';
		}elseif ($host == 'app017') {
			$fedder = 'fd08';
		}elseif ($host == 'app018') {
			$fedder = 'fd09';
		}elseif ($host == 'app019') {
			$fedder = 'fd10';
		}elseif ($host == 'app020') {
			$fedder = 'fd11';
		}elseif ($host == 'app021') {
			$fedder = 'fd12';
		}elseif ($host == 'app022') {
			$fedder = 'fd13';
		}elseif ($host == 'app023') {
			$fedder = 'fd14';
		}elseif ($host == 'app024') {
			$fedder = 'fd15';
		}else {
			$fedder = '';
		}
		return $fedder;
}
public function sendArrival()
{
		$fedder = $this->__cekhostname();
		$result = $this->mongo_db->where(['flag_fed'=>$fedder])->limit(200)->get('arrival');
		print_r('<br>');
		print_r($result);
		if(count($result) != 0)
		{
			$send_data = [];
			$backup = [];
			$idl = [];
			foreach ($result as $v) {
				array_push($idl,$v['tracking_number']);
				array_splice($v,0,1);
				array_push($send_data, $v);
				array_push($backup,array_merge($v,['status_send'=>date('Y-m-d H:i:s')]));
			}
			$this->data = [
							'msg_type'=>'PACKAGE_STATUS',
							'package'=>$send_data
					  	  	];
			$responnya = $this->send_lazada($this->data);
			print_r($responnya);
			$this->mongo_db->insert('respon_lazada',(array)$responnya->response);
			if($responnya)
			{
				$id = $this->mongo_db->insertAll('suksess_send',$backup);
				if($id)
				{
					$deleteStatus = $this->mongo_db->where_in('tracking_number', $idl)->deleteAll('arrival');
				}
			}
		}
}

public function sendSuccess()
{
		$fedder = $this->__cekhostname();
		$result = $this->mongo_db->where(['flag_fed'=>$fedder])->limit(200)->get('success_bc');
		print_r('<br>');
		print_r($result);
		if(count($result) != 0)
		{
			$send_data = [];
			$backup = [];
			$idl = [];
			foreach ($result as $v) {
				array_push($idl,$v['tracking_number']);
				array_splice($v,0,1);
				array_push($send_data, $v);
				array_push($backup,array_merge($v,['status_send'=>date('Y-m-d H:i:s')]));
			}
			$this->data = [
							'msg_type'=>'PACKAGE_STATUS',
							'package'=>$send_data
					  	  	];
			$responnya = $this->send_lazada($this->data);
			print_r($responnya);
			$this->mongo_db->insert('respon_lazada',(array)$responnya->response);
			if($responnya)
			{
				$id = $this->mongo_db->insertAll('suksess_send',$backup);
				if($id)
				{
					$deleteStatus = $this->mongo_db->where_in('tracking_number', $idl)->deleteAll('success_bc');
				}
			}
		}
}
public function sendHandover()
{
		$fedder = $this->__cekhostname();
		$result = $this->mongo_db->where(['flag_fed'=>$fedder])->limit(200)->get('handovered');
		print_r('<br>');
		// print_r($result);
		if(count($result) != 0)
		{
			$send_data = [];
			$backup = [];
			$idl = [];
			foreach ($result as $v) {
				array_push($idl,$v['tracking_number']);
				array_splice($v,0,1);
				array_push($send_data, ['tracking_number'=>$v['tracking_number'],
										'status'=>$v['status'],
										'status_date'=>$v['status_date'],
										'weight_kg'=>$v['weight_kg']
										]);
				array_push($backup,array_merge($v,['status_send'=>date('Y-m-d H:i:s')]));
			}
			$this->data = [
							'msg_type'=>'PACKAGE_STATUS',
							'package'=>$send_data
					  	  	];
			$responnya = $this->send_lazada($this->data);
			print_r($responnya);
			$this->mongo_db->insert('respon_lazada',(array)$responnya->response);
			if($responnya)
			{
				$id = $this->mongo_db->insertAll('suksess_send',$backup);
				if($id)
				{
					$deleteStatus = $this->mongo_db->where_in('tracking_number', $idl)->deleteAll('handovered');
					print_r($deleteStatus);
					print_r($idl);
				}
			}
		}
}


} //end class

/* End of file Feeder.php */
/* Location: ./application/modules/middle_apps/controllers/Feeder.php */