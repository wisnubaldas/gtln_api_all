
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Send_cloud extends MX_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('cloud_model');
		$this->client = new \GuzzleHttp\Client(['cookies' => true]);
	}
	public function send_respon_bc_cloud()
	{
		$data = $this->cloud_model->get_data_respon_bc();
		exit();
		$prepareData = [];
		foreach ($data as $v) {
			$v = array_slice($v, 1);
			array_push($prepareData,$v);
		}
		if(count($data) != 0)
		{
			try{
				$url = 'http://116.206.196.39:8080/api/respon_bc';
				$response = $this->client->request('POST', $url, ['json' => $prepareData]);
				$body = $response->getBody();
				echo $body;
				if(count(json_decode($body)) != 0)
				{
					foreach ($data as $v) 
					{
						$this->cloud_model->update_data_respon(new MongoDB\BSON\ObjectID($v['_id']));
					}				
				}

			}catch (RequestException $e) {
			    	echo Psr7\str($e->getRequest());
			    if ($e->hasResponse()) {
			        echo Psr7\str($e->getResponse());
				}
			}
		}
	}
	public function cloud_lazada_tracking()
	{
		$data = $this->cloud_model->data_sending();
		try{
			$url = 'http://116.206.196.39:8080/api/lazadaNotif/insert_ke_dodo';
			$response = $this->client->request('POST', $url, ['json' => $data]);
			$body = $response->getBody();
			echo $body;
			if($body)
			{
				foreach ($data as $v) {
					$this->mongo_db->where('_id', new MongoDB\BSON\ObjectID($v['_id']))->delete('final_lazada');
				}
			}

		} catch (RequestException $e) {
		    	echo Psr7\str($e->getRequest());
		    if ($e->hasResponse()) {
		        echo Psr7\str($e->getResponse());
			}
		}
	}
	public function prepare_final()
	{
		$data = $this->cloud_model->get_arr();
		$_bc = $this->cloud_model->get_bc($data[0]['hawb']);
		$_hv = $this->cloud_model->get_hv($data[0]['hawb']);
		if(count($data) != 0 && count($_bc) != 0 && count($_hv) != 0)
		{
			$final = array_merge((array)$data[0],(array)$_bc[0],(array)$_hv[0],['flag'=>'cloud']);
			return $this->cloud_model->insert_final($final);
		}else {
			echo 'string';
		}
	}
	public function index()
	{
		$return = [];
		for ($x = 0; $x <= 100; $x++) {
		    $ss = $this->prepare_final();
		    array_push($return,$ss);
		}
		print_r($return); 
	}
}

/* End of file Send_cloud.php */
/* Location: ./application/modules/middle_apps/controllers/Send_cloud.php */