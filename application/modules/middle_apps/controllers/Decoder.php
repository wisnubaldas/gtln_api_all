<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Decoder extends MX_Controller {

	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Macau');
		$this->load->library('mongo_db');
		// Create new connection
		$this->mongo_db->reconnect([
					    'config' => [
					        'connection' => [
					            'host' => ['10.0.0.29'],
					            'port' => [],
					            'user_name' => '',
    							'user_password' => '',
					            'db_name' => 'tracking_lazada',
					        ]
					    ]
					]);		
	}
public function default()
{
	for ($x = 0; $x <= 5; $x++) {
    	$this->ngeloop();
    	sleep(3);
    	echo '<br>';
    	echo date('Y-m-d H:i:s').' sleep(3)';
    	echo '<br>';
	} 
}
private function ngeloop()
{
	if($this->_cronStatus('feeder_default'))
	{
		$this->mongo_db
				->set(['active'=>false,'start'=>date('Y-m-d H:i:s'),'mark'=>'lagi jalan'])
				->where('service','feeder_default')
				->updateAll('log_cron');
		$feeder = $this->mongo_db->get('feeder');
		$hosts_to_ping = $this->_cekFeeder($feeder);
		$jmlServer = $this->mongo_db->count('feeder');
		// ambil data temporari
		$this->arrival = $this->mongo_db->limit(200)->get('tmp_arrival');
		$this->handovered = $this->mongo_db->limit(200)->get('tmp_handovered');
		$this->bc_success = $this->mongo_db->limit(200)->get('tmp_success_bc');
		
		if(count($this->arrival != 0))
		{
			$serverIdup = $jmlServer - $hosts_to_ping;
			$jml = ceil(count($this->arrival)/$serverIdup);
			$i = 0;
			$bagiServer = array_chunk($this->arrival,$jml);
			foreach($bagiServer as $key => $value)
			{
				$i++;
		    	$stat = $feeder[$key];
		    	if ($stat['jobStatus'] == 'idle' && $stat['statusServer'] != 'down') {
						foreach ($value as $v) {
							$xx = array_merge($v,['flag_fed'=>$stat['feeder']]);
							array_splice($xx,0,1);
							$dataI = $this->mongo_db->insert('arrival',$xx);
							print_r($dataI);
							if($dataI){
									$this->mongo_db->where('_id',new MongoDB\BSON\ObjectID($v['_id']))->delete('tmp_arrival');
							}
						
						}
					$this->mongo_db
					    ->set([
					        'jobStatus' => 'proses',
					        'jobStored' => $stat['jobStored']+count($value),
					        'jobStart'  => date('Y-m-d H:i:s'),
					        'statusServer'  => 'on'
					    ])
					    ->where('feeder', $stat['feeder']);
					    $this->mongo_db->updateAll('feeder');
		    	}else {
		    		$this->mongo_db->set([
					        'jobStatus' => 'idle',
					        'statusServer'  => 'off'
					    ])
		    		    ->where('feeder', $stat['feeder']);
					    $this->mongo_db->updateAll('feeder');
		    	}
		    }
		    $this->_log('success','decoder');
		} // bagi data arrival
		if(count($this->handovered) != 0)
		{
			$serverIdup = $jmlServer - $hosts_to_ping;
			$jml = ceil(count($this->handovered)/$serverIdup);
			$bagiServer = array_chunk($this->handovered,$jml);
			$i = 0;
			foreach($bagiServer as $key => $value)
		    {
		    	$i++;
		    	$stat = $feeder[$key];
		    	if ($stat['jobStatus'] == 'idle' && $stat['statusServer'] != 'down') {
						foreach ($value as $v) {
							$xx = array_merge($v,['flag_fed'=>$stat['feeder']]);
							array_splice($xx,0,1);
							$dataI = $this->mongo_db->insert('handovered',$xx);
							print_r($dataI);
							if($dataI){
								$this->mongo_db->where('_id',new MongoDB\BSON\ObjectID($v['_id']))->delete('tmp_handovered');
							}
							
						}
					$this->mongo_db
					    ->set([
					        'jobStatus' => 'proses',
					        'jobStored' => $stat['jobStored']+count($value),
					        'jobStart'  => date('Y-m-d H:i:s'),
					        'statusServer'  => 'on'
					    ])
					    ->where('feeder', $stat['feeder']);
					    $this->mongo_db->updateAll('feeder');
		    	}else {
		    		$this->mongo_db->set([
					        'jobStatus' => 'idle',
					        'statusServer'  => 'off'
					    ])
		    		    ->where('feeder', $stat['feeder']);
					    $this->mongo_db->updateAll('feeder');
		    	}
		    }
		    $this->_log('success','decoder');
		}// bagi data H/O
		if(count($this->bc_success) != 0)
		{
			$serverIdup = $jmlServer - $hosts_to_ping;
			$jml = ceil(count($this->bc_success)/$serverIdup);
			$bagiServer = array_chunk($this->bc_success,$jml);
			$i = 0;
			foreach($bagiServer as $key => $value)
		    {
		    	$i++;
		    	$stat = $feeder[$key];
		    	if ($stat['jobStatus'] == 'idle' && $stat['statusServer'] != 'down') {
						foreach ($value as $v) {
							$xx = array_merge($v,['flag_fed'=>$stat['feeder']]);
							array_splice($xx,0,1);
							$dataI = $this->mongo_db->insert('success_bc',$xx);
							print_r($dataI);
							if($dataI){
								$this->mongo_db->where('_id',new MongoDB\BSON\ObjectID($v['_id']))->delete('tmp_success_bc');
							}
							
						}
					$this->mongo_db
					    ->set([
					        'jobStatus' => 'proses',
					        'jobStored' => $stat['jobStored']+count($value),
					        'jobStart'  => date('Y-m-d H:i:s'),
					        'statusServer'  => 'on'
					    ])
					    ->where('feeder', $stat['feeder']);
					    $this->mongo_db->updateAll('feeder');
		    	}else {
		    		$this->mongo_db->set([
					        'jobStatus' => 'idle',
					        'statusServer'  => 'off'
					    ])
		    		    ->where('feeder', $stat['feeder']);
					    $this->mongo_db->updateAll('feeder');
		    	}
		    }
		    $this->_log('success','decoder');
		} // bagi bc_success
	}

	// balikin status cronjob nya
	$this->mongo_db
		    ->set(['active'=>true,'end'=>date('Y-m-d H:i:s'),'mark'=>'udah selesai'])
		    	->where('service','feeder_default')
				->updateAll('log_cron');	
}

	public function arrival()
	{
		if($this->_cronStatus('feeder_arrival'))
		{
			$this->mongo_db
				->set(['active'=>false,'activedate'=>date('Y-m-d H:i:s'),'mark'=>'lagi jalan'])
				->where('service','feeder_arrival')
				->updateAll('log_cron');
				$feeder = $this->mongo_db->get('feeder');
				$hosts_to_ping = $this->_cekFeeder($feeder);
				$data = $this->mongo_db->limit(700)->get('tmp_arrival');
				$jmlServer = $this->mongo_db->count('feeder');
				// $jmlServerAktif = $this->mongo_db->where('jobStatus','proses')->get('feeder');
				$serverIdup = $jmlServer - $hosts_to_ping;
				$jml = ceil(count($data)/$serverIdup);
				if(count($data) != 0)
				{
					$i = 0;
					$bagiServer = array_chunk($data,$jml);
					foreach($bagiServer as $key => $value)
					    {
					    	$i++;
					    	$stat = $feeder[$key];
					    	if ($stat['jobStatus'] == 'idle' && $stat['statusServer'] != 'down') {
									foreach ($value as $v) {
										$xx = array_merge($v,['flag_fed'=>$stat['feeder']]);
										array_splice($xx,0,1);
										$dataI = $this->mongo_db->insert('arrival',$xx);
										print_r($dataI);
										if($dataI){
												$this->mongo_db->where('_id',new MongoDB\BSON\ObjectID($v['_id']))->delete('tmp_arrival');
										}
									
									}
								$this->mongo_db
								    ->set([
								        'jobStatus' => 'proses',
								        'jobStored' => $stat['jobStored']+count($value),
								        'jobStart'  => date('Y-m-d H:i:s'),
								        'statusServer'  => 'on'
								    ])
								    ->where('feeder', $stat['feeder']);
								    $this->mongo_db->updateAll('feeder');
					    	}else {
					    		$this->mongo_db->set([
								        'jobStatus' => 'idle',
								        'statusServer'  => 'off'
								    ])
					    		    ->where('feeder', $stat['feeder']);
								    $this->mongo_db->updateAll('feeder');
					    	}
					    }
					    $this->_log('success','decoder');
					    
				}

		}// end status cron
		$this->mongo_db
		    ->set(['active'=>true,'activedate'=>date('Y-m-d H:i:s'),'mark'=>'udah selesai'])
		    	->where('service','feeder_arrival')
				->updateAll('log_cron');
	} // end function
public function handovered()
	{
		if($this->_cronStatus('feeder_handovered'))
		{
			$this->mongo_db
				->set(['active'=>false,'activedate'=>date('Y-m-d H:i:s'),'mark'=>'lagi jalan'])
				->where('service','feeder_handovered')
				->updateAll('log_cron');
				$feeder = $this->mongo_db->get('feeder');
				$hosts_to_ping = $this->_cekFeeder($feeder);
				$data = $this->mongo_db->limit(700)->get('tmp_handovered');
				$jmlServer = $this->mongo_db->count('feeder');
				// $jmlServerAktif = $this->mongo_db->where('jobStatus','proses')->get('feeder');
				$serverIdup = $jmlServer - $hosts_to_ping;
				$jml = ceil(count($data)/$serverIdup);
				if(count($data) != 0)
				{
					$i = 0;
					$bagiServer = array_chunk($data,$jml);
					foreach($bagiServer as $key => $value)
					    {
					    	$i++;
					    	$stat = $feeder[$key];
					    	if ($stat['jobStatus'] == 'idle' && $stat['statusServer'] != 'down') {
									foreach ($value as $v) {
										$xx = array_merge($v,['flag_fed'=>$stat['feeder']]);
										array_splice($xx,0,1);
										$dataI = $this->mongo_db->insert('handovered',$xx);
										if($dataI){
											$this->mongo_db->where('_id',new MongoDB\BSON\ObjectID($v['_id']))->delete('tmp_handovered');
										}
										
									}
								$this->mongo_db
								    ->set([
								        'jobStatus' => 'proses',
								        'jobStored' => $stat['jobStored']+count($value),
								        'jobStart'  => date('Y-m-d H:i:s'),
								        'statusServer'  => 'on'
								    ])
								    ->where('feeder', $stat['feeder']);
								    $this->mongo_db->updateAll('feeder');
					    	}else {
					    		$this->mongo_db->set([
								        'jobStatus' => 'idle',
								        'statusServer'  => 'off'
								    ])
					    		    ->where('feeder', $stat['feeder']);
								    $this->mongo_db->updateAll('feeder');
					    	}
					    }
					    $this->_log('success','decoder');
				
				}

		}// end status cron
		$this->mongo_db
		    ->set(['active'=>true,'activedate'=>date('Y-m-d H:i:s'),'mark'=>'udah selesai'])
		    	->where('service','feeder_handovered')
				->updateAll('log_cron');
	} // end function
public function bc_success()
	{
		if($this->_cronStatus('feeder_success'))
		{
			$this->mongo_db
				->set(['active'=>false,'activedate'=>date('Y-m-d H:i:s'),'mark'=>'lagi jalan'])
				->where('service','feeder_success')
				->updateAll('log_cron');
				$feeder = $this->mongo_db->get('feeder');
				$hosts_to_ping = $this->_cekFeeder($feeder);
				$data = $this->mongo_db->limit(700)->get('tmp_success_bc');
				$jmlServer = $this->mongo_db->count('feeder');
				// $jmlServerAktif = $this->mongo_db->where('jobStatus','proses')->get('feeder');
				$serverIdup = $jmlServer - $hosts_to_ping;
				$jml = ceil(count($data)/$serverIdup);
				if(count($data) != 0)
				{
					$i = 0;
					$bagiServer = array_chunk($data,$jml);
					foreach($bagiServer as $key => $value)
				    {
				    	$i++;
				    	$stat = $feeder[$key];
				    	if ($stat['jobStatus'] == 'idle' && $stat['statusServer'] != 'down') {
								foreach ($value as $v) {
									$xx = array_merge($v,['flag_fed'=>$stat['feeder']]);
									array_splice($xx,0,1);
									$dataI = $this->mongo_db->insert('success_bc',$xx);
									if($dataI){
										$this->mongo_db->where('_id',new MongoDB\BSON\ObjectID($v['_id']))->delete('tmp_success_bc');
									}
									
								}
							$this->mongo_db
							    ->set([
							        'jobStatus' => 'proses',
							        'jobStored' => $stat['jobStored']+count($value),
							        'jobStart'  => date('Y-m-d H:i:s'),
							        'statusServer'  => 'on'
							    ])
							    ->where('feeder', $stat['feeder']);
							    $this->mongo_db->updateAll('feeder');
				    	}else {
				    		$this->mongo_db->set([
							        'jobStatus' => 'idle',
							        'statusServer'  => 'off'
							    ])
				    		    ->where('feeder', $stat['feeder']);
							    $this->mongo_db->updateAll('feeder');
				    	}
				    }
				    $this->_log('success','decoder');
					    
				}

		}// end status cron
		$this->mongo_db
					    ->set(['active'=>true,'activedate'=>date('Y-m-d H:i:s'),'mark'=>'udah selesai'])
					    	->where('service','feeder_success')
							->updateAll('log_cron');
	} // end function

	private function _cekFeeder($host = array())
	{
		$x = 0;
		for($i = 0; $i < count($host); $i++)
		{
			$xxx = exec(sprintf('ping -c 1 -W 5 %s', escapeshellarg($host[$i]['localIP'])), $res, $rval);
			if($rval !== 0)
			{
				$x++;
				$this->mongo_db->set(['statusServer' => 'down'])->where('feeder',$host[$i]['feeder']);
				$this->mongo_db->updateAll('feeder');
			}else {
				$this->mongo_db->set(['statusServer' => 'on'])->where('feeder',$host[$i]['feeder']);
				$this->mongo_db->updateAll('feeder');
			}
		}
		return $x;
	}
	private function _log($log,$message)
	{

		$this->mongo_db->insert('log_decoder',['log'=>$log,'message'=>$message,'date'=>date('Y-m-d H:i:s')]);
	}
	private function _cronStatus($data)
	{
		$log_cron = $this->mongo_db
					->where('service',$data)
					->getOne('log_cron');
		return $log_cron[0]['active'];
	}

}

/* End of file Decoder.php */
/* Location: ./application/modules/middle_apps/controllers/Decoder.php */