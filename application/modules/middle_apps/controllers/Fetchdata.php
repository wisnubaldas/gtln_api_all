<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Fetchdata extends MX_Controller {
	private $log_respon;
	public function __construct()
	{
		parent::__construct();
		$this->client = new \GuzzleHttp\Client(['cookies' => true]);
		$this->load->model('fetch_clearance');
		$model = ['tr_gatein_model','bc_respone_400_model','tr_gateout_model'];
		$this->load->model($model);
		// Include library
		$this->load->library('mongo_db');
		$this->mongo_db->reconnect([
					    'config' => [
					        'connection' => [
					            'host' => ['10.0.0.29'],
					            'port' => [],
					            'user_name' => '',
    							'user_password' => '',
					            'db_name' => 'tracking_lazada',
					        ]
					    ]
					]);
		date_default_timezone_set('Asia/Macau');
	}

	public function index()
	{
		$dtScan = $this->tr_gatein_model
				->on('GTLN')
				->where(['istransfer'=>0])
				->with_bc_t_shipment('fields:hawb')
				->fields('hawb,tglscan,idkey,status')
				->order_by('rand()')
				->limit(100)
				->get_all();
		
		if(!$dtScan)
		{
			$this->log_respon = ["response"=>"arrival data not found","dateCreate"=>date('Y-m-d')];
			$this->mongo_db->insert('log',$this->log_respon);
			exit();
		}
		$this->data_arrived = [];
		foreach ($dtScan as $v) {
			// jika tidak ada host di shipment update gatein flag 5, jika ada lanjut
			if(!property_exists($v,'bc_t_shipment'))
				{
					$update_data = array('istransfer'=>5);
					$this->tr_gatein_model
							->on('GTLN')
							->where('idkey',$v->idkey)
							->update($update_data);
				}elseif (strpos($v->hawb, 'i') !== false){ // cari hawb yg ada id kecil
					$update_data = array('hawb'=>strtoupper($v->hawb));
					$this->tr_gatein_model
							->on('GTLN')
							->where('idkey',$v->idkey)
							->update($update_data);
				}elseif ($v->status != 'ARR') {
					$update_data = array('istransfer'=>5);
					$this->tr_gatein_model
							->on('GTLN')
							->where('idkey',$v->idkey)
							->update($update_data);
				}
				else{
					$tracking_number = $v->hawb;
					$status = 'arrived';
					$status_date = strtotime($v->tglscan);
					array_push($this->data_arrived, compact('tracking_number','status','status_date'));
				}
		}
		$this->tr_gatein_model->reset_connection();
		$insertIds = $this->mongo_db->insertAll('tmp_arrival', $this->data_arrived);
		print_r($insertIds);

		if(count($insertIds) != 0)
		{
			foreach ( $this->data_arrived as $v) {
			$this->tr_gatein_model
							->on('GTLN')
							->where('hawb',$v['tracking_number'])
							->update(['istransfer'=>1]);
							$this->tr_gatein_model->reset_connection();
			}
		$this->mongo_db->insert('log',['respon'=>implode(",",$insertIds),"dateCreate"=>strtotime('now')]);
		}
	}

	public function success_bc()
	{
		//cari data arrival yg sudah terkirim status tracking di flag 1 gatein
		$dtScan = $this->bc_respone_400_model
					->on('GTLN')
					->where(['flag_lzd'=>0,'KD_RESPON'=>403]) // 400 belom terkirim
					->fields('NO_BARANG,KD_RESPON,WK_REKAM,id_res_cn')
					->order_by('rand()')
					->limit(300)
					->get_all();
					// print_r($dtScan);
		if(!$dtScan)
		{
			$this->log_respon = ["response"=>"success_bc data not found","dateCreate"=>strtotime('now')];
			$this->mongo_db->insert('log',$this->log_respon);
			exit();
		}
		$this->bc = [];
		foreach ($dtScan as $v) {
	 		$tracking_number = $v->NO_BARANG;
			$status = 'import_custom_clearance_success';
			$status_date = strtotime($v->WK_REKAM);
			array_push($this->bc, compact('tracking_number','status','status_date'));
		}
		$insertIds = $this->mongo_db->insertAll('tmp_success_bc', $this->bc);
		if(count($insertIds) != 0)
		{
			foreach ( $dtScan as $v) {
			$this->bc_respone_400_model
							->on('GTLN')
							->where('id_res_cn',$v->id_res_cn)
							->update(['flag_lzd'=>1]);
							$this->bc_respone_400_model->reset_connection();
			}
		$this->mongo_db->insert('log',['respon'=>implode(",",$insertIds),"dateCreate"=>strtotime('now')]);
		}
	}
	public function hand_over()
	{
		
		$dtScan = $this->tr_gateout_model
					->on('GTLN')
					->where(['istransfer'=>0])
					->with_bc_t_shipment('fields:Weight')
					->fields('hawb,scanout,id')
					->order_by('rand()')
					->limit(300)
					->get_all();
					$this->tr_gateout_model->reset_connection();
		
		if(!$dtScan)
		{
			$this->log_respon = ["response"=>"handovered data not found","dateCreate"=>strtotime('now')];
			$this->mongo_db->insert('log',$this->log_respon);
			exit();
		}
		$this->handovered = [];
		foreach ($dtScan as $v) {
			if(!property_exists($v,'bc_t_shipment'))
				{
					$update_data = array('istransfer'=>5);
					$this->tr_gateout_model
							->on('GTLN')
							->where('id',$v->id)
							->update($update_data);
				}elseif (!property_exists($v->bc_t_shipment,'Weight')){ 
					$update_data = array('istransfer'=>5);
					$this->tr_gateout_model
							->on('GTLN')
							->where('id',$v->id)
							->update($update_data);
				}elseif ($v->bc_t_shipment->Weight == '') {
					$update_data = array('istransfer'=>5);
					$this->tr_gateout_model
							->on('GTLN')
							->where('id',$v->id)
							->update($update_data);
				}
				else{
					$tracking_number = $v->hawb;
					$status = 'handovered';
					$status_date = strtotime($v->scanout);
					$weight_kg = $v->bc_t_shipment->Weight;
					array_push($this->handovered, compact('tracking_number','status','status_date','weight_kg'));
				}
		}
		$insertIds = $this->mongo_db->insertAll('tmp_handovered', $this->handovered);
		print_r($insertIds);

		if(count($insertIds)!= 0)
		{
			foreach ($this->handovered as $v) {
			$this->tr_gateout_model
							->on('GTLN')
							->where('hawb',$v['tracking_number'])
							->update(['istransfer'=>1]);
							$this->tr_gateout_model->reset_connection();
			}
		$this->mongo_db->insert('log',['respon'=>implode(",",$insertIds),"dateCreate"=>strtotime('now')]);
		}
	}
}

/* End of file Fetchdata.php */
/* Location: ./application/modules/middle_apps/controllers/Fetchdata.php */