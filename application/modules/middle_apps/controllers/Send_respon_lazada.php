<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Send_respon_lazada extends MX_Controller {

private $url = 'https://crossborder.lazada.com/api/3pl/v2/?'; // production lazada
private $user_id = 'GTLN';
private $key = '0NEOFNLDFJDK1';
private $action = 'StatusUpdate';
private $log_respon;
public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Macau');
		$this->load->library('mongo_db');
		// Create new connection
		$this->mongo_db->connect();
		$this->client = new \GuzzleHttp\Client(['cookies' => true]);
		$this->date_time = $this->atom_date(date('Y-m-d H:i:s'));
		
	}
	// cari nilai array
	private function multi_array_search($search_for, $search_in) {
	    foreach ($search_in as $element) {
	        if ( ($element === $search_for) ){
	            return true;
	        }elseif(is_array($element)){
	            $result = $this->multi_array_search($search_for, $element);
	            if($result == true)
	                return true;
	        }
	    }
	    return false;
	}
	// modifikasi waktu sending
	private function _modify_datetime($date,$modif)
	{
		$date = date('Y-m-d H:i:s',$date);
		$date = new DateTime($date);
		$date->modify($modif.' hours');
		return $date->format('Y-m-d H:i:s');
	}
	// kirim arrival kelazada
	public function sending()
	{
	$data = $this->mongo_db->select(['tracking_number','status','status_date'])->limit(200)->get('tmp_arrival');
	$send_data = [];
		if(count($data) != 0)
		{
			foreach ($data as $v) {
				$v = array_slice($v,1); // buang _id
				$cek1 = $this->mongo_db->select(['status','status_date'])
							->where('tracking_number',$v['tracking_number'])
							->get('case_timestamp');
					if(count($cek1) != 0)
					{
						$bc = $this->multi_array_search('import_custom_clearance_success',$cek1);
						$hv = $this->multi_array_search('handovered',$cek1);
							if ($bc == true && $hv == false) {
								// echo 'nga ada hv ada bc';
								$status_date = $this->_modify_datetime($cek1[0]['status_date'],'-1');
							}
							if ($bc == false && $hv == true) {
								// echo 'ngga ada bc ada hv';
								$status_date = $this->_modify_datetime($cek1[0]['status_date'],'-2');
							}
							if ($bc == true && $hv == true) {
								// echo 'ada dua dua nya';
								$this->_modify_datetime($cek1[1]['status_date'],'-2');
							}
					}else {
						$status_date = date('Y-m-d H:i:s',$v['status_date']);
					}
					$v = array_merge($v,compact('status_date'));
					array_push($send_data, $v);
				}// end 
			$this->data = [
							'msg_type'=>'PACKAGE_STATUS',
							'package'=>$send_data
					  	  	];
			$responnya = $this->send_lazada($this->data);
			print_r($responnya);

			if ($responnya->response->success == TRUE) {
				if ($responnya) {
					$data_merge = [];
					$time_sending = strtotime($responnya->response->request_time);
					foreach ($send_data as $v) {
						array_push($data_merge,array_merge($v,['time_sending'=>$time_sending,'flag'=>'lazada']));
					}
					$id = $this->mongo_db->insertAll('send_arrival',$data_merge);
					// print_r($data_merge);
					
					if($id)
					{
						foreach ($send_data as $v) {
							$deleteStatus = $this->mongo_db->where('tracking_number',$v['tracking_number'])->delete('tmp_arrival');
						}
						
					}
				}
			}else {
			$this->mongo_db->insert('log_respon_lazada',["response"=>$responnya->response,
										'success'=>$responnya->response->success,
										'remark'=>'arrival']);
			}
		}
	}
	// kirim 403 ke lazada
	public function send_bc_success()
	{
		$data = $this->mongo_db->select(['tracking_number','status','status_date'])->limit(200)->get('tmp_success_bc');

		if(count($data) != 0)
		{
			$send_data = [];
			foreach ($data as $v) {
				$v = array_slice($v,1);

				$cek1 = $this->mongo_db
							->where('tracking_number',$v['tracking_number'])
							->get('send_arrival');

				if(count($cek1) != 0 )
				{

					// $status_date = $this->_modify_datetime(strtotime($cek1[0]['status_date']),'+1');
					$status_date = date('Y-m-d H:i:s',$v['status_date']);

				}else {
					$this->mongo_db->insert('case_timestamp',$v);
					$status_date = date('Y-m-d H:i:s',$v['status_date']);
				}
				$v = array_merge($v,compact('status_date'));
				array_push($send_data, $v);
			}
			$this->data = [
							'msg_type'=>'PACKAGE_STATUS',
							'package'=>$send_data
					  	  	];
			$responnya = $this->send_lazada($this->data);
			print_r($responnya);

			if ($responnya->response->success == TRUE) {
				if ($responnya) {
					$data_merge = [];
					$time_sending = strtotime($responnya->response->request_time);
					foreach ($send_data as $v) {
						array_push($data_merge,array_merge($v,['time_sending'=>$time_sending,'flag'=>'lazada']));
					}
					$id = $this->mongo_db->insertAll('send_bc_success',$data_merge);
					// print_r($data_merge);
					if($id)
					{
						foreach ($send_data as $v) {
							$deleteStatus = $this->mongo_db->where('tracking_number',$v['tracking_number'])->delete('tmp_success_bc');
						}
						
					}
				}
			}else {
			$this->mongo_db->insert('log_respon_lazada',["response"=>$responnya->response,
										'success'=>$responnya->response->success,
										'remark'=>'bc_success']);
			}
		}
	}
	public function send_handovered()
	{
		$send_data = [];
		$data = $this->mongo_db->select(['tracking_number','status','status_date','weight_kg'])->limit(200)->get('tmp_handovered');
		if(count($data) != 0)
		{
			foreach ($data as $v) {
				$v = array_slice($v,1);
				$cek1 = $this->mongo_db
							->where('tracking_number',$v['tracking_number'])
							->get('send_arrival');
				if(count($cek1) == 0)
				{
					$cek2 = $this->mongo_db
							->where('tracking_number',$v['tracking_number'])
							->get('send_bc_success');
					if(count($cek2) == 0)
					{
						$this->mongo_db->insert('case_timestamp',array_slice($v,-1));
						$status_date = date('Y-m-d H:i:s',$v['status_date']);
					}else {
						// $status_date = $this->_modify_datetime(strtotime($cek2[0]['status_date']),'+1');
						$status_date = date('Y-m-d H:i:s',$v['status_date']);
					}
				}else {
					// $status_date = $this->_modify_datetime(strtotime($cek1[0]['status_date']),'+2');
					$status_date = date('Y-m-d H:i:s',$v['status_date']);
				}
				$v = array_merge($v,compact('status_date'));
				array_push($send_data, $v);

			}
			$this->data = [
							'msg_type'=>'PACKAGE_STATUS',
							'package'=>$send_data
					  	  	];
			$responnya = $this->send_lazada($this->data);
			print_r($responnya);
			if ($responnya->response->success == TRUE) {
				if ($responnya) {
					$data_merge = [];
					$time_sending = strtotime($responnya->response->request_time);
					foreach ($send_data as $v) {
						array_push($data_merge,array_merge($v,['time_sending'=>$time_sending,'flag'=>'lazada']));
					}
					$id = $this->mongo_db->insertAll('send_handovered',$data_merge);
					// print_r($data_merge);
					if($id)
					{
						foreach ($send_data as $v) {
							$deleteStatus = $this->mongo_db->where('tracking_number',$v['tracking_number'])->delete('tmp_handovered');
						}
						
					}
				}
			}else {
			$this->mongo_db->insert('log_respon_lazada',["response"=>$responnya->response,
										'success'=>$responnya->response->success,
										'remark'=>'bc_success']);
			}
		}
		
	}


private function atom_date($date)
	{
		$datetime = new DateTime($date,new DateTimeZone("Asia/Hong_Kong"));
		return $datetime->format(DateTime::ATOM); // Updated ISO8601
	}
private function get_digest ($data, $key) {
				return hash_hmac('sha256', $data, $key, false);
	}
private function send_lazada($data)
	{
		// // generate signatur for login
				$string_to_sign = urlencode('action='.$this->action.'&timestamp='.$this->date_time.'&userid='.$this->user_id);
				$signatur = $this->get_digest($string_to_sign, $this->key);
				$url_query = [
								'action'=>$this->action,
								'timestamp'=>$this->date_time,
								'userid'=>$this->user_id,
								'signature'=>$signatur
							 ];
				$url = $this->url.http_build_query($url_query);
		try{
			$response = $this->client->request('POST', $url,['body' => json_encode($data),'timeout' => 0],['debug' => true]);
			$stream = $response->getBody();
			$contents = $stream->getContents(); // returns all the contents
				return json_decode($contents);
					} catch (RequestException $e) {
					    	// echo Psr7\str($e->getRequest());
					    	log_message('error', $e->getRequest());
					    if ($e->hasResponse()) {
					        // echo Psr7\str($e->getResponse());
					        // echo $e->getResponse();
					        log_message('error', $e->getResponse());
					    }
					}
	}

} // end class


/* End of file Send_respon_lazada.php */
/* Location: ./application/modules/middle_apps/controllers/Send_respon_lazada.php */