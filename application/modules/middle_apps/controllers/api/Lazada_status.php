<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Lazada_status extends REST_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->mongo_db->reconnect([
					    'config' => [
					        'connection' => [
					            'host' => ['10.0.0.29'],
					            'port' => [],
					            'user_name' => '',
    							'user_password' => '',
					            'db_name' => 'tracking_lazada',
					        ]
					    ]
					]);
	}

	public function arrivalData_post()
	{
		$this->data_arrived = [];
		$data = $this->post();
		$respon = [];
		foreach ($data as $v) {
			$tracking_number = $v['hawb'];
			$status = 'arrived';
			$status_date = $v['tglscan'];
			array_push($this->data_arrived, compact('tracking_number','status','status_date'));
			array_push($respon,$v);
		}
		 $insertIds = $this->mongo_db->insertAll('tmp_arrival', $this->data_arrived);
//		 /// ini harus di return id table arrived
		 $this->set_response('dasdasdasd', 200);
	}

	public function bcSuccess_post()
	{
		$data = $this->post();
		$respon = [];
		$this->bc = [];
		foreach ($data as $v) {
	 		$tracking_number = $v['NO_BARANG'];
			$status = 'import_custom_clearance_success';
			$status_date = str_replace('/','-',$v['WK_REKAM']);
			array_push($this->bc, compact('tracking_number','status','status_date'));
			array_push($respon,$v['NO_BARANG']);
		}
		$insertIds = $this->mongo_db->insertAll('tmp_success_bc', $this->bc);
		 // ini harus di return dari table bc respon
		 $this->set_response($respon, 200);
	}
	public function handovered_post()
	{
		$respon = [];
		$data = $this->post();
		$this->handovered = [];
		foreach ($data as $v) {
					$tracking_number = $v['hawb'];
					$status = 'handovered';
					$status_date = $v['scanout'];
					$weight_kg = $v['bc_t_shipment']['Weight'];
					array_push($this->handovered, compact('tracking_number','status','status_date','weight_kg'));
					array_push($respon,$v['id']);
		}
		$insertIds = $this->mongo_db->insertAll('tmp_handovered', $this->handovered);
		$this->set_response($respon, 200);
	}

}

/* End of file Lazada_status.php */
/* Location: ./application/modules/middle_apps/controllers/api/Lazada_status.php */