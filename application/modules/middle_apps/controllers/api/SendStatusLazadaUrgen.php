<?php
/**
 * Created by PhpStorm.
 * User: wisnu
 * Date: 1/17/18
 * Time: 11:16 AM
 * kirim data status tracking sesuai dengan permintaan data ke Lazada
 * (data di request dari si lazada)
 */
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH."modules/bc_clearance/controllers/Parsing_xml.php";
class SendStatusLazadaUrgen extends MX_Controller
{
    protected $user = "nararya123";
    protected $password = "567123";
    protected $token = "TnpNNU56ZzFOVGd5TFRFNA0K";
    protected $npwp = "739785582031000";
    protected $wsdl = APPPATH.'wsdl/WSBarangKirimanNew.wsdl';
    protected $client;
    protected $dataGet = "";

    public function __construct()
    {
        parent::__construct();
        $this->__conectMongo();
        $this->load->model('Tb_bc_respon','respon400');
        $this->load->model('Tr_gatein_model','tGatein');
        $this->load->model('Tr_gateout_model','tGateout');
        $this->setting = array(
            'stream_context'=> stream_context_create(array('ssl'=> array(
                    'verify_peer'=>false,
                    'verify_peer_name'=>false,
                    'allow_self_signed' => true
                )
                )
            )
        );
        $this->client = new SoapClient($this->wsdl, $this->setting);
        $this->idSender = $this->user.'^$'.$this->password;
    }

    private function __conectMongo()
    {
        $this->mongo_db->reconnect([
            'config' => [
                'connection' => [
                    'host' => ['192.168.88.22'],
                    'port' => [],
                    'user_name' => '',
                    'user_password' => '',
                    'db_name' => 'tracking_lazada',
                ]
            ]
        ]);
    }
    public function get_data()
    {
        if($this->input->get('services') == 'arr')
        {
            $datas = [];
            $adaArr = [];
            $data = $this->mongo_db
                ->select(['hawb'],['_id'])
                ->limit(300)
                ->where_type('arrStat.status','null')
                ->get('col_dataDadakan');
            print_r($data);
            echo '<br>';
            if($data)
            {
                foreach ($data as $v)
                {
                    array_push($datas,$v['hawb']);
                }
                $gIn = $this->tGatein->on('GTLN')->where('hawb',$datas)->fields('hawb,tglscan')->as_array()->get_all();
//                var_dump($gIn);
                if($gIn)
                {
                    foreach ($gIn as $v)
                    {
                        $this->mongo_db
                            ->set(['arrStat.status'=>'arrived','flag'=>true,'arrStat.dateArr'=>$v['tglscan']])
                            ->where('hawb',$v['hawb'])
                            ->update('col_dataDadakan');
//                        print_r($gIn);
                        array_push($adaArr,$v['hawb']);
                    }
                }
                $nullArr = array_values(array_diff($datas,$adaArr));
                if(!empty($nullArr))
                {
                    echo "<br>";
                    var_dump($nullArr);
                    $this->mongo_db
                        ->set(['arrStat.status'=>false,'arrStat.dateArr'=>null])
                        ->where_in('hawb',$nullArr);
                    $this->mongo_db
                        ->updateAll('col_dataDadakan');
                }
            }
        }

        if($this->input->get('services') == 'bcsukses')
        {

            $data = $this->mongo_db
                ->select(['hawb'],['_id'])
                ->limit(300)
                ->where_exists(['statSucc'=>false])
                ->get('col_dataDadakan');
            $adaSucc = [];
            $datas = [];
            foreach ($data as $v)
            {
                array_push($datas,$v['hawb']);
            }


            if($datas)
            {
                print_r($datas);
                echo '<br>';
                $this->mongo_db->switch_db('db_log_barangkiriman');
                $data403 = $this->mongo_db
                    ->select(['NO_BARANG','WK_REKAM'],['_id'])
                    ->where('KD_RESPON','403')
                    ->where_in('NO_BARANG',$datas)
                    ->get('tmp_bc_response');
            }
//            print_r($datas);
//                $g400 = $this->respon400
//                        ->on('GTLN')
//                        ->where('NO_BARANG',$datas)
//                        ->where('KD_RESPON',403)
//                        ->fields('NO_BARANG,WK_REKAM')
//                        ->get_all();
                        if($data403)
                        {
                            $this->mongo_db->switch_db('tracking_lazada');
                            foreach ($data403 as $v)
                            {
                                $this->mongo_db
                                    ->set(['statSucc.status'=>'import_custom_clearance_success','statSucc.dateSucc'=>$v['WK_REKAM']])
                                    ->where('hawb',$v['NO_BARANG'])
                                    ->update('col_dataDadakan');
                                array_push($adaSucc,$v['NO_BARANG']);
//                                print_r(array_unique($v));
//                                echo '<br>';
                            }
                        }
            $nullStat = array_values(array_diff($datas,array_unique($adaSucc)));
            if(!empty($nullStat))
            {
                $this->mongo_db->switch_db('tracking_lazada');
                $this->mongo_db
                    ->set(['statSucc.status'=>null,'statSucc.dateSucc'=>null])
                    ->where_in('hawb',$nullStat);
                $this->mongo_db
                    ->updateAll('col_dataDadakan');
                print_r($nullStat);
            }
//            print_r($nullStat);

        }


// cek hand over
        if($this->input->get('services') == 'handover')
        {
            $data = $this->mongo_db
                ->select(['hawb'],['_id'])
                ->limit(300)
                ->where_type('statHand.status','null')
                ->get('col_dataDadakan');

            $adaHo = [];
            $datas = [];
            foreach ($data as $v)
            {
                array_push($datas,$v['hawb']);
            }

            $gOut = $this->tGateout
                        ->on('GTLN')
                ->fields('hawb,scanout')
                ->with_bc_t_shipment_bk01('fields:Weight')
                        ->where('hawb',$datas)
                        ->as_array()
                        ->get_all();
//            $this->mongo_db->switch_db('db_log_barangkiriman');
//            $gOut = $this->mongo_db
//                ->select(['NO_BARANG','WK_REKAM'],['_id'])
//                ->where('KD_RESPON','403')
//                ->where_in('NO_BARANG',$datas)
//                ->get('tmp_bc_response');
            print_r($gOut);
            echo "<br>";
            if($gOut)
            {
                foreach ($gOut as $v)
                {
                    if(!array_key_exists('bc_t_shipment_bk01',$v))
                    {
                        $this->mongo_db
                                ->set(['statHand.status'=>'handovered','statHand.dateHo'=>$v['scanout'],'statHand.weight'=>null])
                                ->where('hawb',$v['hawb']);
                        $this->mongo_db
                                ->updateAll('col_dataDadakan');
                            print_r($v);
                    }else{
                        if($v['bc_t_shipment_bk01'] == NULL)
                        {
                            $this->mongo_db
                                ->set(['statHand.status'=>'handovered','statHand.dateHo'=>$v['scanout'],'statHand.weight'=>null])
                                ->where('hawb',$v['hawb']);
                            $this->mongo_db
                                ->updateAll('col_dataDadakan');
                            print_r($v);
                        }else{
                            $this->mongo_db
                                ->set(['statHand.status'=>'handovered','statHand.dateHo'=>$v['scanout'],'statHand.weight'=>$v['bc_t_shipment_bk01']['Weight']])
                                ->where('hawb',$v['hawb']);
                            $this->mongo_db
                                ->updateAll('col_dataDadakan');
                            print_r($v);
                        }
                    }
                    array_push($adaHo,$v['hawb']);
                }
            }
            $nullStat = array_values(array_diff($datas,$adaHo));
            if(!empty($nullStat))
            {
                $this->mongo_db
                    ->set(['statHand.status'=>false,'statHand.dateHo'=>null,'statHand.weight'=>null])
                    ->where_in('hawb',$nullStat);
                $this->mongo_db
                    ->updateAll('col_dataDadakan');
                print_r($v);
            }

        }
    }
}