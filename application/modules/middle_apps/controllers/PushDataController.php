<?php
defined('BASEPATH') OR exit('No direct script access allowed');
	/** ############
	 **	PushDataController.php create by: wisnu baldas
	 ** dir: /home/wisnu/Documents/web/gtln2.0/application/modules/middle_apps/controllers/PushDataController.php
     *  ############
     * client api dari bawah ke cloud
     **/
class PushDataController extends MX_Controller {
	public $client;

	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Macau');
		$this->client = new \GuzzleHttp\Client(['cookies' => true]);
		$this->load->model('fetch_clearance');
		$model = ['tr_gatein_model','bc_respone_400_model','tr_gateout_model'];
		$this->load->model($model);
		$this->mongo_db->reconnect([
					    'config' => [
					        'connection' => [
					            'host' => ['192.168.88.22'],
					            'port' => [],
					            'user_name' => '',
    							'user_password' => '',
					            'db_name' => 'tracking_lazada',
					        ]
					    ]
					]);
	}

	private function _cronStatus()
	{
		$log_cron = $this->mongo_db
					->where('service','pushArrival')
					->getOne('log_cron');
		return $log_cron[0]['active'];
	}
// public function pushArrival()
// {
// 	for ($x = 0; $x <= 3; $x++) {
//     			$this->lopinganArr();
//     			sleep(5);
//     			// echo PHP_EOL;
//     			// echo "sleep".date('Y-m-d H:i:s');
//     			// echo PHP_EOL;
// 		}
// }
public function pushArrival()
	{
			$dtScan = $this->tr_gatein_model
				->on('GTLN')
				->where(['istransfer'=>0,'status'=>'ARR'])
				->with_bc_t_shipment('fields:hawb')
				->fields('hawb,tglscan,idkey,status')
				// ->order_by('rand()')
				->limit(300)
//                ->as_array()
				->get_all();
            print_r($dtScan);

            if(!$dtScan)
            {
                // update status cron
                $this->mongo_db
                    ->set(['mark'=>'data kosong','activedate'=>date('Y-m-d H:i:s')])
                    ->where('service','pushArrival')
                    ->updateAll('log_cron');
                exit();
            }
			$dataArr =  [];
			foreach ($dtScan as $v) {
			// jika tidak ada host di shipment update gatein flag 5, jika ada lanjut
			if(!property_exists($v,'bc_t_shipment'))
				{
					$update_data = array('istransfer'=>5);
					$this->tr_gatein_model
							->on('GTLN')
							->where('idkey',$v->idkey)
							->update($update_data);
				}elseif (strpos($v->hawb, 'i') !== false){ // cari hawb yg ada id kecil
					$update_data = array('hawb'=>strtoupper($v->hawb));
					$this->tr_gatein_model
							->on('GTLN')
							->where('idkey',$v->idkey)
							->update($update_data);
				}
				else{
					array_push($dataArr, (object)$v);
				}
			}

			try {
	            $r = $this->client->request('POST', 'http://116.206.197.1/gtln1/middle_apps/api/lazada_status/arrivalData', ['json' => $dataArr]);
					$dataID = $r->getBody();
					 echo $dataID;
	        }catch (Exception $ex) {
		            echo $ex->getResponse()->getBody();
		            echo $ex->getResponse()->getStatusCode();
	        }
	        if($dataID)
	        {
	        	$idNya = json_decode($dataID);
	        	print_r($idNya);

	        	$r = $this->tr_gatein_model
						->on('GTLN')
						->where('hawb',$idNya)
						->update(['istransfer'=>1]);
				
					$this->mongo_db
					    ->set(['active'=>true,'activedate'=>date('Y-m-d H:i:s'),'mark'=>'udah selesai'])
					    	->where('service','pushArrival')
							->updateAll('log_cron');
				
				// print_r($r);
	        }
		
	}
public function pushBcsuccess()
{
	for ($x = 0; $x <= 2; $x++) {

    			$this->lopinganBcsuccess();
    			sleep(5);
    			echo '<br>';
    			echo "sleep".date('Y-m-d H:i:s');
    			echo '<br>';
		}
}
private function lopinganBcsuccess()
	{

	  		$dtScan = $this->bc_respone_400_model
					->on('GTLN')
					->where(['flag_lzd'=>0,'KD_RESPON'=>403]) // 400 belom terkirim
					->fields('NO_BARANG,KD_RESPON,WK_REKAM,id_res_cn')
					// ->order_by('rand()')
					->limit(200)
					->get_all();
			print_r($dtScan);
			if(!$dtScan)
			{
				// update status cron
				$this->mongo_db
					    ->set(['mark'=>'data kosong','activedate'=>date('Y-m-d H:i:s')])
					    ->where('service','pushBcsuccess')
						->updateAll('log_cron');
						exit();				
			}

			try {
            $r = $this->client->request('POST', 'http://116.206.197.1/gtln1/middle_apps/api/lazada_status/bcSuccess', ['json' => $dtScan]);
				$dataID = $r->getBody();
				$idNya = json_decode($dataID);
		        	$r = $this->bc_respone_400_model
									->on('GTLN')
									->where('NO_BARANG',$idNya)
									->update(['flag_lzd'=>1]);
		        }catch (Exception $ex) {
			            echo $ex->getResponse()->getBody();
			            echo $ex->getResponse()->getStatusCode();
		        }
		
		}
		
	
	public function pushHandovered()
	{
			$dtScan = $this->tr_gateout_model
					->on('GTLN')
					->where(['istransfer'=>0])
					->with_bc_t_shipment('fields:Weight')
					->fields('hawb,scanout,id')
					// ->order_by('rand()')
					->limit(300)
					->get_all();

			print_r($dtScan);
			if(!$dtScan)
			{
				// update status cron
				$this->mongo_db
					    ->set(['mark'=>'data kosong','activedate'=>date('Y-m-d H:i:s')])
					    ->where('service','pushHandovered')
						->updateAll('log_cron');
						exit();				
			}

		try {
            $r = $this->client->request('POST', 'http://116.206.197.1/gtln1/middle_apps/api/lazada_status/handovered', ['json' => $dtScan]);
				$dataID = $r->getBody();
				
				// echo $dataID;

        }catch (Exception $ex) {
	            echo $ex->getResponse()->getBody();
	            echo $ex->getResponse()->getStatusCode();
        }

        if($dataID)
        {
        	$idNya = json_decode($dataID);
        	print_r($idNya);
			$r = $this->tr_gateout_model
							->on('GTLN')
							->where('id',$idNya)
							->update(['istransfer'=>1]);
			if($r)
				{
					$this->mongo_db
					    ->set(['active'=>true,'activedate'=>date('Y-m-d H:i:s'),'mark'=>'udah selesai'])
					    	->where('service','pushHandovered')
							->updateAll('log_cron');
				}
			print_r($r);
        }
		
	}

}

/* End of file PushDataController.php */
/* Location: ./application/modules/middle_apps/controllers/PushDataController.php */