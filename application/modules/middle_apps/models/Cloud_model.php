
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cloud_model extends MY_Model {

	public function __construct()
	{
		parent::__construct();
		$this->mongo_db->reconnect([
					    'config' => [
					        'connection' => [
					            'host' => ['localhost'],
					            'port' => [],
					            'user_name' => '',
    							'user_password' => '',
					            'db_name' => 'tracking_lazada',
					        ]
					    ]
					]);
	}
	public function update_data_respon($id)
	{
		$this->mongo_db->switch_db('db_log_barangkiriman');
		$this->mongo_db
			    ->set([
			        'flag'=>'cloud'
			    ])
			    ->where('_id', $id);
		return $this->mongo_db->update('tmp_bc_response');	
	}
	public function get_data_respon_bc()
	{
		$this->mongo_db->switch_db('db_log_barangkiriman');
		return $result = $this->mongo_db
			   ->limit(300)
               ->where('flag', 'mysql')
               ->get('tmp_bc_response');

	}
	public function data_sending()
	{
		return $result = $this->mongo_db
					->limit(100)
					->get('final_lazada');
	}
	public function insert_final($data)
	{
		return $this->mongo_db->insert('final_lazada',$data);
	}
	public function update_lazada($data)
	{
		$this->mongo_db
			    ->set(['flag'=>'cloud'])
			    ->where_in('tracking_number', $data);
			$this->mongo_db->updateAll('send_arrival');
		$this->mongo_db
			    ->set(['flag'=>'cloud'])
			    ->where_in('tracking_number', $data);
			$this->mongo_db->updateAll('send_bc_success');
		$this->mongo_db
			    ->set(['flag'=>'cloud'])
			    ->where_in('tracking_number', $data);
			$this->mongo_db->updateAll('send_handovered');
	}
	public function get_hv($data)
	{
		$result = $this->mongo_db
					->where('tracking_number',$data)
					->getOne('send_bc_success');
		$dataBc = [];
		foreach ($result as $v) {
		 		$hawb = $v['tracking_number'];
				$ts_handover_kirim = $v['status_date'];
				$ts_handover_respon = $v['time_sending'];
				array_push($dataBc,compact('hawb','ts_handover_kirim','ts_handover_respon'));
			 }
		return $dataBc;
	}
	public function get_bc($data)
	{
		$result = $this->mongo_db
					->where('tracking_number',$data)
					->getOne('send_handovered');
		$dataBc = [];
		foreach ($result as $v) {
			 	$hawb = $v['tracking_number'];
				$ts_success_kirim = $v['status_date'];
				$ts_success_respon = $v['time_sending'];
				array_push($dataBc,compact('hawb','ts_success_kirim','ts_success_respon'));
			 }
		return $dataBc;
	}
	public function get_arr()
	{
		$dataArr = [];
		$xx = $this->mongo_db
						->where('flag','lazada')
						->getOne('send_arrival');
		foreach ($xx as $v) {
			$hawb = $v['tracking_number'];
			$ts_arr_kirim = $v['status_date'];
			$ts_arr_respon = $v['time_sending'];
			array_push($dataArr,compact('hawb','ts_arr_kirim','ts_arr_respon'));
			$this->mongo_db->set('flag', 'cloud');
			$this->mongo_db->where('_id',new MongoDB\BSON\ObjectID($v['_id']))->update('send_arrival');
		}
		return $dataArr;

		// if (count($xx) != 0) {
		// 	$bc = $this->mongo_db
		// 			->where_in('tracking_number',$tr)
		// 			->get('send_bc_success');
		// 	$dataBc = [];
		// 	foreach ($dataArr as $i=>$v) {
		// 	 	$hawb = $bc[$i]['tracking_number'];
		// 		$ts_success_kirim = $bc[$i]['status_date'];
		// 		$ts_success_respon = $bc[$i]['time_sending'];
		// 		$xxx = array_merge($dataArr[$i],compact('hawb','ts_success_kirim','ts_success_respon'));
		// 		array_push($dataBc,$xxx);
		// 	 }
		// 	 if(count($bc) != 0)
		// 	 {
		// 	 	$hv = $this->mongo_db
		// 			->where_in('tracking_number',$tr)
		// 			->get('send_handovered');
		// 		$dataHv = [];
		// 		print_r(count($hv));
		// 		foreach ($hv as $i=>$v) {
					// print_r($v);
				 // 	$hawb = $v['tracking_number'];
					// $ts_handover_kirim = $v['status_date'];
					// $ts_handover_respon = $v['time_sending'];
					// array_push($dataHv,compact('hawb','ts_handover_kirim','ts_handover_respon'));
				 // }
				 // print_r($dataHv);
			 // }
		// }
		// $dataFinal = [];
		// foreach ($dataArr as $i=>$v) {
			

		// 	// $xx = array_merge($dataArr[$i],$dataBc[$i]);
		// 	// array_push($dataFinal,array_merge($xx);
		// }
		// return ['data'=>$dataFinal,'hawb'=>$tr];	

		


	}

}

/* End of file Cloud_model.php */
/* Location: ./application/modules/middle_apps/models/Cloud_model.php */