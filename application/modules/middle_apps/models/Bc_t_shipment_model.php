<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bc_t_shipment_model extends MY_Model {
public function __construct()
{
	$this->table = 'bc_t_shipment';
        $this->primary_key = 'hawb';
        //$this->has_one['details'] = 'User_details_model';
        // $this->has_one['details'] = array('User_details_model','user_id','id');
        
        $this->has_one['tr_gatein'] = array('local_key'=>'hawb', 'foreign_key'=>'hawb', 'foreign_model'=>'Tr_gatein_model');
        $this->has_one['tr_getout'] = array('foreign_model'=>'tr_getout_model','foreign_table'=>'tr_getout','foreign_key'=>'hawb','local_key'=>'hawb');
        $this->return_as = 'object';
        
	parent::__construct();
	//Do your magic here
}



}

/* End of file t_shipment_model.php */
/* Location: ./application/modules/lzd/models/t_shipment_model.php */