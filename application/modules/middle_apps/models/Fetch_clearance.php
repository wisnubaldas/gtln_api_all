<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Fetch_clearance extends CI_Model {

public function __construct()
{
	
	parent::__construct();
	$this->GTLN = $this->load->database('GTLN', TRUE);
	

}

function get_data_arrival()
{
	return $this->GTLN->select('hawb,tglscan')
			->from('tr_gatein')
			->where('istransfer',0)
			->limit(10)
			->get()->result();
}
	

}

/* End of file Fetch_clearance.php */
/* Location: ./application/modules/middle_apps/models/Fetch_clearance.php */