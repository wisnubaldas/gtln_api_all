<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tr_gateout_model extends MY_Model {
public function __construct()
{
		$this->table = 'tr_gateout';
        $this->primary_key = 'hawb';
        $this->has_one['tr_gatein'] = array('foreign_model'=>'Tr_gatein_model','foreign_table'=>'tr_gatein','foreign_key'=>'hawb','local_key'=>'hawb');
        $this->has_one['bc_respone_400'] = array('foreign_model'=>'bc_respone_400_model','foreign_table'=>'bc_respone_400','foreign_key'=>'NO_BARANG','local_key'=>'hawb');
        $this->has_one['bc_t_shipment'] = array('foreign_model'=>'bc_t_shipment_model','foreign_table'=>'bc_t_shipment','foreign_key'=>'hawb','local_key'=>'hawb');
        $this->has_many['tr_pkg_status'] = array('foreign_model'=>'tr_pkg_status_model','foreign_table'=>'tr_pkg_status','foreign_key'=>'hawb','local_key'=>'hawb');
        $this->return_as = 'object';
        $this->timestamps = FALSE;
	parent::__construct();
	
}
	

}

/* End of file Tr_gateout_model.php */
/* Location: ./application/modules/lzd/models/Tr_gateout_model.php */