<?php
defined('BASEPATH') OR exit('No direct script access allowed');



/* End of file js_css.php */
/* Location: ./application/helpers/js_css.php */
if (!function_exists('js_data')) {
    function js_data()
{
    return $js = [
                'jquery'=>'jquery.js',
                'bootstrap/js'=>'bootstrap.js',
                'bootstrap-select/js'=>'bootstrap-select.js',
                'jquery-slimscroll'=>'jquery.slimscroll.js',
                'node-waves'=>'waves.js',
                'jquery-countto'=>'jquery.countTo.js',
                'raphael'=>'raphael.min.js',
                'chartjs'=>'Chart.bundle.js',
                'flot-charts'=>'jquery.flot.js',
                'flot-charts'=>'jquery.flot.resize.js',
                'flot-charts'=>'jquery.flot.pie.js',
                'flot-charts'=>'jquery.flot.categories.js',
                'flot-charts'=>'jquery.flot.time.js',
                'jquery-sparkline'=>'jquery.sparkline.js',
                'jquery-validation'=>'jquery.validate.js',
                
                ];
}
}
if (!function_exists('css_data')) {
    function css_data()
    {
        return $css = [
                    'bootstrap/css'=>'bootstrap.css',
                    'node-waves'=>'waves.css',
                    'morrisjs'=>'morris.css',
                ];
    }
}
